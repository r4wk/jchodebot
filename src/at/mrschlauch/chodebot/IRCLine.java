package at.mrschlauch.chodebot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IRCLine {
    private String raw;
    private String prefix;
    private String nick;
    private String username;
    private String host;
    private SenderType senderType;
    private String command;
    private List<String> params;
    private String trailing;

    public IRCLine(String raw) {
        this.raw = raw;

        LinkedList<String> parts = new LinkedList<String>(Arrays.asList(raw
                .split(" ")));

        if (parts.get(0).startsWith(":")) {
            this.prefix = parts.removeFirst().substring(1);

            int ex = this.prefix.indexOf("!");
            int at = this.prefix.indexOf("@");

            if (ex != -1 && at != -1 && at > ex) {
                this.nick = this.prefix.substring(0, ex);
                this.username = this.prefix.substring(ex + 1, at);
                this.host = this.prefix.substring(at + 1);
                this.senderType = SenderType.USER;
            } else {
                this.senderType = SenderType.SERVER;
            }
        }

        this.command = parts.removeFirst();
        this.params = new ArrayList<String>();

        StringBuilder sb = null;
        for (String part : parts) {
            if (sb != null) {
                sb.append(" ");
                sb.append(part);
            } else if (part.startsWith(":")) {
                sb = new StringBuilder();
                sb.append(part.substring(1));
            } else if (!part.isEmpty()) {
                this.params.add(part);
            } else {
                // ignore, extra space between params
            }
        }

        if (sb != null) {
            this.trailing = sb.toString();
        }
    }

    public String getParam(int i) {
        return this.params.get(i);
    }

    public int getParamCount() {
        return this.params.size();
    }
}
