package at.mrschlauch.chodebot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Channel {
    private Connection connection;
    private String name;
    private String topic;
    private Map<Character, String> modes = new HashMap<Character, String>();
    private String topicSetter;
    private long createTime;
    private long topicSetTime;
    private boolean synced;

    private Set<User> users = new HashSet<User>();
    private Set<User> voiced = new HashSet<User>();
    private Set<User> halfOps = new HashSet<User>();
    private Set<User> ops = new HashSet<User>();
    private Set<User> superOps = new HashSet<User>();
    private Set<User> owners = new HashSet<User>();

    public Channel(Connection conn, String name) {
        this.connection = conn;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("<Channel %s>", this.name);
    }

    public void addUser(User user, String statuses) {
        assert !this.users.contains(user) : String.format(
                "User %s already is on channel %s!", user, this.name);
        this.users.add(user);

        for (int i = 0; i < statuses.length(); ++i) {
            char c = statuses.charAt(i);

            if (c == 'v') {
                this.voiced.add(user);
            } else if (c == 'h') {
                this.halfOps.add(user);
            } else if (c == 'o') {
                this.ops.add(user);
            } else if (c == 'a') {
                this.superOps.add(user);
            } else if (c == 'q') {
                this.owners.add(user);
            }
        }
    }

    public void removeUser(User user) {
        assert this.users.contains(user) : String.format(
                "User %s is not on channel %s!", user, this.name);
        this.users.remove(user);
        this.voiced.remove(user);
        this.halfOps.remove(user);
        this.ops.remove(user);
        this.superOps.remove(user);
        this.owners.remove(user);
    }

    public void handleMode(ModeInfo info) {
        if (info.getType() == ModeType.STATUS) {
            User user = info.getUser();
            char mode = info.getMode();

            if (info.isAdding()) {
                if (mode == 'v') {
                    this.voiced.add(user);
                } else if (mode == 'h') {
                    this.halfOps.add(user);
                } else if (mode == 'o') {
                    this.ops.add(user);
                } else if (mode == 'a') {
                    this.superOps.add(user);
                } else if (mode == 'q') {
                    this.owners.add(user);
                }
            } else {
                if (mode == 'v') {
                    this.voiced.remove(user);
                } else if (mode == 'h') {
                    this.halfOps.remove(user);
                } else if (mode == 'o') {
                    this.ops.remove(user);
                } else if (mode == 'a') {
                    this.superOps.remove(user);
                } else if (mode == 'q') {
                    this.owners.remove(user);
                }
            }
        } else if (info.getType() != ModeType.LIST) {
            if (info.isAdding()) {
                this.modes.put(info.getMode(), info.getArg());
            } else {
                this.modes.remove(info.getMode());
            }
        }
    }
}