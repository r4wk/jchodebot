package at.mrschlauch.chodebot;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import at.mrschlauch.chodebot.events.ActionEvent;
import at.mrschlauch.chodebot.events.CTCPEvent;
import at.mrschlauch.chodebot.events.CTCPResponseEvent;
import at.mrschlauch.chodebot.events.ChannelModeEvent;
import at.mrschlauch.chodebot.events.ConnectEvent;
import at.mrschlauch.chodebot.events.DisconnectEvent;
import at.mrschlauch.chodebot.events.EventRegistry;
import at.mrschlauch.chodebot.events.JoinEvent;
import at.mrschlauch.chodebot.events.KickEvent;
import at.mrschlauch.chodebot.events.MessageEvent;
import at.mrschlauch.chodebot.events.NickEvent;
import at.mrschlauch.chodebot.events.NoticeEvent;
import at.mrschlauch.chodebot.events.NumericEvent;
import at.mrschlauch.chodebot.events.PartEvent;
import at.mrschlauch.chodebot.events.QuitEvent;
import at.mrschlauch.chodebot.events.UnknownCommandEvent;
import at.mrschlauch.chodebot.events.UserModeEvent;

import lombok.Getter;
import lombok.Setter;

public class Connection {
    private static final int BUFFER_SIZE = 4096;

    @Getter private Bot bot;
    @Getter private String currentNick;
    @Getter private String chanTypes = "#&";
    @Getter private Map<Character, Character> statusToPrefix = new HashMap<Character, Character>();
    @Getter private Map<Character, Character> prefixToStatus = new HashMap<Character, Character>();
    @Getter private String[] chanModes = new String[] { "", "", "", "" };
    @Getter private Set<Character> userModes = new HashSet<Character>();
    @Getter private boolean hasWHOX;
    @Getter private UserTracker userTracker = new UserTracker(this);
    @Getter @Setter private boolean reconnecting = true;
    private Socket socket;
    private OutputStream out;
    private InputStream in;
    private LinkedList<DelayedFuncContainer> delayedFuncs = new LinkedList<DelayedFuncContainer>();
    private LinkedList<String> sendQueue = new LinkedList<String>();
    private List<String> linesToProcess = new LinkedList<String>();
    private byte[] previousData = new byte[0];
    private byte[] readBuffer = new byte[BUFFER_SIZE];

    public Connection(Bot bot) {
        this.bot = bot;
        this.currentNick = this.bot.getConfig().getConnectionInfo().getNick();
        this.statusToPrefix.put('v', '+');
        this.statusToPrefix.put('o', '@');
        this.prefixToStatus.put('+', 'v');
        this.prefixToStatus.put('@', 'o');
    }
    
    public void connect() {
        try {
            this.socket = new Socket(this.bot.getConfig().getConnectionInfo()
                    .getServer(), this.bot.getConfig().getConnectionInfo()
                    .getPort());
            this.socket.setSoTimeout(300);
            this.out = this.socket.getOutputStream();
            this.in = this.socket.getInputStream();
        } catch (UnknownHostException uhe) {
            Bot.logger.severe("Cannot connect to server: unknown host");
            return;
        } catch (IOException ioe) {
            Bot.logger.severe("Cannot connect to server:");
            Bot.logger.throwing("Connection", "connect", ioe);
            return;
        }

        this.scheduleFunc(new IDelayedFunc() {
            @Override
            public boolean call() {
                Connection.this.processSendQueue();
                return true;
            }
        }, 1000);
        this.scheduleFunc(new IDelayedFunc() {
            @Override
            public boolean call() {
                Connection.this.userTracker.invalidateAccounts();
                return true;
            }

        }, 60 * 1000); // 1 minute

        EventRegistry.getInstance().fire(new ConnectEvent(this));
        doClientRegistration();
        readLoop();
    }

    private void doClientRegistration() {
        // makes the server send all prefixes in /names and /who replies, as
        // opposed to just the highest one. -crucial- for properly keeping track
        // of statuses in channels
        // without it:
        // 1. user X has status +ao
        // 2. our join
        // 3. receive /names reply saying user X has status +a
        // 4. user X is desuperopped, leaving them with status +o
        //    to us: no status left
        // with it:
        // 1. user X has status +ao
        // 2. our join
        // 3. receive /names reply aying user X has status +ao
        // 4. user X is desuperopped, leaving them with status +o
        //    to us: we still know about +o from /names :>
        
        ConnectionInfo info = this.bot.getConfig().getConnectionInfo();
        
        sendNow("CAP REQ :multi-prefix");
        sendNow("CAP END");

        if (info.getServerPassword() != null) {
            sendNow(String.format("PASS %s", info.getServerPassword()));
        }

        sendNow(String.format("NICK %s", this.currentNick));
        sendNow(String.format("USER %s %s * :%s",
                info.getUsername(),
                info.getServer(),
                info.getRealname()));
    }
    
    private void readLoop() {
        while (true) {
            this.processDelayedFuncs();
            readOnce();
            if (socket.isClosed()) {
                EventRegistry.getInstance().fire(new DisconnectEvent(this));
                break;
            }
        }
    }
    
    private void readOnce() {
        int nread = 0;
        
        try {
            nread = this.in.read(this.readBuffer);
        } catch (SocketTimeoutException e) {
            return;
        } catch (IOException e) {
            Bot.logger.severe("Read error: " + e);
            try { this.socket.close(); } catch (IOException e1) {}
            return;
        }
        
        if (nread == -1) {
            // end of stream
            Bot.logger.info("End of stream.");
            try { this.socket.close(); } catch (IOException e) {}
            return;
        }
        
        // if no data was received, SocketTimeoutException would have been thrown
        assert nread != 0;
        
        byte[] allData = new byte[previousData.length + nread];
        System.arraycopy(previousData, 0, allData, 0, previousData.length);
        System.arraycopy(readBuffer, 0, allData, previousData.length, nread);
        
        int idx = Utils.lastIndexOf(allData, (byte) '\n');
        
        if (idx == -1) {
            previousData = allData;
            return;
        }
        
        String linesString = Utils.decode(allData, 0, idx + 1);
        String[] rawLines = linesString.split("\r\n");

        for (String rawLine : rawLines) {
            System.out.println(rawLine);
            IRCLine line = new IRCLine(rawLine);
            processLine(line);
        }

        previousData = new byte[allData.length - idx - 1];
        System.arraycopy(allData, idx + 1, previousData, 0, previousData.length);
    }
    
    public void scheduleFunc(IDelayedFunc func, long delay) {
        this.delayedFuncs.add(new DelayedFuncContainer(func, delay));
    }

    public void unscheduleFunc(IDelayedFunc func) {
        for (ListIterator<DelayedFuncContainer> it = this.delayedFuncs
                .listIterator(); it.hasNext();) {
            DelayedFuncContainer container = it.next();
            if (container.getFunc() == func) {
                it.remove();
            }
        }
    }

    private void processDelayedFuncs() {
        for (ListIterator<DelayedFuncContainer> it = this.delayedFuncs
                .listIterator(); it.hasNext();) {
            DelayedFuncContainer container = it.next();

            if (!container.isDue()) {
                continue;
            }

            boolean reschedule = false;

            try {
                reschedule = container.getFunc().call();
            } catch (Exception e) {
                Bot.logger.severe("Error calling delayed function");
                Bot.logger.throwing("Connection", "processDelayedFuncs", e);
            }

            if (reschedule) {
                it.set(container.copy());
            } else {
                it.remove();
            }

        }
    }

    public void processSendQueue() {
        if (!this.sendQueue.isEmpty()) {
            this.sendNow(this.sendQueue.pop());
        }
    }

    private void processLine(IRCLine line) {
        switch (line.getCommand()) {
        case "PING":
            this.sendNow(String.format("PONG %s", line.getTrailing()));
            break;
        case "PRIVMSG":
            this.processPrivmsg(line);
            break;
        case "NOTICE":
            this.processNotice(line);
            break;
        case "NICK":
            this.processNick(line);
            break;
        case "JOIN":
            this.processJoin(line);
            break;
        case "KICK":
            this.processKick(line);
            break;
        case "PART":
            this.processPart(line);
            break;
        case "QUIT":
            this.processQuit(line);
            break;
        case "MODE":
            this.processMode(line);
        case "INVITE":
            this.processInvite(line);
        default:
            int numeric;
            try {
                numeric = Integer.parseInt(line.getCommand());
            } catch (NumberFormatException e) {
                EventRegistry.getInstance().fire(new UnknownCommandEvent(line));
                return;
            }
            switch (numeric) {
            case 5:
                this.processISUPPORT(line);
                break;
            case 376:
                this.processENDOFMOTD(line);
                break;
            case 353:
                this.processNAMREPLY(line);
                break;
            case 366:
                this.processENDOFNAMES(line);
                break;
            case 324:
                this.processCHANNELMODEIS(line);
                break;
            case 331:
                this.processNOTOPIC(line);
                break;
            case 332:
                this.processTOPIC(line);
                break;
            case 333:
                this.processTOPICWHOTIME(line);
                break;
            case 329:
                this.processCREATIONTIME(line);
                break;
            case 352:
                this.processWHOREPLY(line);
                break;
            case 354:
                this.processWHOSPCRPL(line);
                break;
            }
            EventRegistry.getInstance().fire(new NumericEvent(line, numeric));
        }
    }

    private void processWHOSPCRPL(IRCLine line) {
        String username = line.getParams().get(1);
        String host = line.getParams().get(2);
        String nick = line.getParams().get(3);
        String nsAccount = line.getParams().get(4);
        String realname = line.getTrailing();

        User user = this.userTracker.getUser(nick);

        if (user == null) {
            // they must have quit before WHOSPCRPL
            return;
        }

        if (!user.isSynced()) {
            user.setUsername(username);
            user.setHost(host);
            user.setRealname(realname);
            user.setNickservAccount(nsAccount);
            user.setSynced(true);
            user.setSyncTime(System.currentTimeMillis());
            this.userTracker.assignAccount(user);
        }
    }

    private void processWHOREPLY(IRCLine line) {
        String nick = line.getParams().get(5);
        String username = line.getParams().get(2);
        String host = line.getParams().get(3);
        String realname = line.getTrailing().substring(
                line.getTrailing().indexOf(" ") + 1);

        User user = this.userTracker.getUser(nick);

        if (user == null) {
            // they must have quit before WHOREPLY
            return;
        }

        if (!user.isSynced()) {
            user.setUsername(username);
            user.setHost(host);
            user.setRealname(realname);
            user.setNickservAccount("0");
            user.setSynced(true);
            user.setSyncTime(System.currentTimeMillis());
            this.userTracker.assignAccount(user);
        }

    }

    private void processCREATIONTIME(IRCLine line) {
        String channelName = line.getParams().get(1);
        long createTime = Long.parseLong(line.getParams().get(2));

        Channel channel = this.userTracker.getChannel(channelName);

        if (channel == null) {
            // we already left that channel
            return;
        }

        channel.setCreateTime(createTime);
    }

    private void processTOPICWHOTIME(IRCLine line) {
        String channelName = line.getParams().get(1);
        String setter = line.getParams().get(2);
        long time = Long.parseLong(line.getParams().get(3));

        Channel channel = this.userTracker.getChannel(channelName);

        if (channel == null) {
            // receiving topic info for channel we already left
            return;
        }

        channel.setTopicSetter(setter);
        channel.setTopicSetTime(time);
    }

    private void processNOTOPIC(IRCLine line) {
        String channelName = line.getParams().get(1);
        Channel channel = this.userTracker.getChannel(channelName);

        if (channel == null) {
            // receiving NOTOPIC for channel we already left
            return;
        }

        channel.setTopic(null);
    }

    private void processTOPIC(IRCLine line) {
        String channelName = line.getParams().get(1);
        String topic = line.getTrailing();

        Channel channel = this.userTracker.getChannel(channelName);

        if (channel == null) {
            return; // we already left that channel
        }

        channel.setTopic(topic);
    }

    private void processCHANNELMODEIS(IRCLine line) {
        String channelName = line.getParam(1);

        Channel channel = this.userTracker.getChannel(channelName);

        if (channel == null) {
            return; // received CHANMODEIS for channel we already left
        }

        String modeString = line.getParam(2);
        LinkedList<String> args = new LinkedList<String>();

        for (int i = 3; i < line.getParamCount(); ++i) {
            args.add(line.getParam(i));
        }

        List<ModeInfo> modeInfos = this.parseCmodes(modeString, args);

        for (ModeInfo info : modeInfos) {
            channel.handleMode(info);
        }
    }

    private void processENDOFNAMES(IRCLine line) {
        String channelName = line.getParams().get(1);
        Channel channel = this.userTracker.getChannel(channelName);
        // since NAMES is sent immediately after joining a channel
        // before any other activity
        assert channel != null;

        if (!channel.isSynced()) {
            channel.setSynced(true);
        }
    }

    private void processNAMREPLY(IRCLine line) {
        String channelName = line.getParams().get(2);
        Channel channel = this.userTracker.getChannel(channelName);

        // since NAMES is sent immediately after joining a channel
        // before any other activity
        assert channel != null;

        if (channel.isSynced()) {
            // already received NAMREPLY
            return;
        }

        String[] prefixedNicks = line.getTrailing().split(" ");

        for (String prefixedNick : prefixedNicks) {
            StringBuilder statusesBuilder = new StringBuilder();
            StringBuilder nickBuilder = new StringBuilder(prefixedNick);

            while (this.prefixToStatus.containsKey(nickBuilder.charAt(0))) {
                char prefix = nickBuilder.charAt(0);
                char status = this.prefixToStatus.get(prefix);
                statusesBuilder.append(status);
                nickBuilder.deleteCharAt(0);
            }
            
            String nick = nickBuilder.toString();
            String statuses = statusesBuilder.toString();

            User user = this.userTracker.getUser(nick);

            if (user == null) {
                user = this.userTracker.makeUser(nick);
                this.userTracker.trackUser(user);
            }

            this.userTracker.joinChannel(channel, user, statuses);
        }
    }

    private void processInvite(IRCLine line) {
        Object inviter;
        if (line.getSenderType() == SenderType.SERVER) {
            inviter = line.getPrefix();
        } else {
            User user = this.userTracker.getUser(line.getNick());

            if (user == null) {
                // private notice or message without join
                user = this.userTracker.makeUser(line.getNick(),
                        line.getUsername(), line.getHost());
                this.userTracker.trackUser(user);
            } else if (user.getUsername() == null) {
                // we received an invite from a channel user before receiving
                // WHO
                user.setUsername(line.getUsername());
                user.setHost(line.getHost());
            } else if (!user.getUsername().equals(line.getUsername())
                    || !user.getHost().equals(line.getHost())) {
                // PM or new hidden host message
                if (user.getChannels().size() == 0) {
                    // PM
                    this.userTracker.untrackUser(user);
                    user = this.userTracker.makeUser(line.getNick(),
                            line.getUsername(), line.getHost());
                    this.userTracker.trackUser(user);
                } else {
                    // new hidden host
                    assert user.getUsername().equals(line.getUsername()) : "Impersonation!";
                    user.setHost(line.getHost());
                    user.setSynced(false);
                    this.userTracker.assignAccount(user);
                }
            }

            inviter = user;
        }

        String channelName = line.getTrailing();

        if (channelName == null) {
            channelName = line.getParams().get(1);
        }

        // fire invite event
    }

    private void processMode(IRCLine line) {
        String target = line.getParams().get(0);

        String modeString;
        LinkedList<String> args = new LinkedList<String>();

        if (line.getTrailing() != null) {
            modeString = line.getTrailing();
        } else {
            List<String> params = line.getParams();
            modeString = params.get(1);

            for (int i = 2; i < params.size(); ++i) {
                args.add(params.get(i));
            }
        }

        if (target.equals(line.getPrefix())) {
            // user mode
            for (ModeInfo info : this.parseUmodes(modeString)) {
                if (info.isAdding()) {
                    this.userModes.add(info.getMode());
                } else {
                    this.userModes.remove(info.getMode());
                }

                EventRegistry.getInstance().fire(new UserModeEvent(line, target, info));
            }
        } else {
            // channel mode
            // mode setter can be server or chanserv (from outside channel) :(

            Object setter;
            if (line.getSenderType() == SenderType.SERVER) {
                setter = line.getPrefix();
            } else {
                User user = this.userTracker.getUser(line.getNick());

                if (user == null) {
                    // chanserv for example
                    // treat like PM
                    user = this.userTracker.makeUser(line.getNick(),
                            line.getUsername(), line.getHost());
                    this.userTracker.trackUser(user);
                } else if (line.getUsername() == null) {
                    user.setUsername(line.getUsername());
                    user.setHost(line.getHost());
                } else if (!line.getHost().equals(user.getHost())) {
                    // new hidden host :(
                    assert user.getUsername().equals(line.getUsername());
                    user.setHost(line.getHost());
                    user.setSynced(false);
                    this.userTracker.assignAccount(user);
                }

                setter = user;
            }

            Channel channel = this.userTracker.getChannel(target);
            assert channel != null;

            for (ModeInfo info : this.parseCmodes(modeString, args)) {
                channel.handleMode(info);
                EventRegistry.getInstance().fire(new ChannelModeEvent(line, target, info, channel, setter));
            }
        }
    }

    private List<ModeInfo> parseUmodes(String modeString) {
        boolean adding = true;
        List<ModeInfo> modeInfos = new ArrayList<ModeInfo>();

        for (int i = 0; i < modeString.length(); ++i) {
            char c = modeString.charAt(i);

            if (c == '+') {
                adding = true;
            } else if (c == '-') {
                adding = false;
            } else {
                ModeInfo info = new ModeInfo(ModeType.UMODE, adding, c);
                modeInfos.add(info);
            }
        }
        return modeInfos;
    }

    private List<ModeInfo> parseCmodes(String modeString,
            LinkedList<String> args) {
        boolean adding = true;
        List<ModeInfo> modeInfos = new ArrayList<ModeInfo>();

        for (int i = 0; i < modeString.length(); ++i) {
            char c = modeString.charAt(i);
            String s = Character.toString(c);

            if (c == '+') {
                adding = true;
            } else if (c == '-') {
                adding = false;
            } else if (this.chanModes[ModeType.LIST.ordinal()].contains(s)) {
                String arg = args.removeFirst();
                ModeInfo info = new ModeInfo(ModeType.LIST, adding, c, arg);
                modeInfos.add(info);
            } else if (this.chanModes[ModeType.ARG_ALWAYS.ordinal()]
                    .contains(s)) {
                String arg = args.removeFirst();
                ModeInfo info = new ModeInfo(ModeType.ARG_ALWAYS, adding, c,
                        arg);
                modeInfos.add(info);
            } else if (this.chanModes[ModeType.ARG_ON_SET.ordinal()]
                    .contains(s)) {
                String arg;

                if (adding) {
                    arg = args.removeFirst();
                } else {
                    arg = null;
                }

                ModeInfo info = new ModeInfo(ModeType.ARG_ALWAYS, adding, c,
                        arg);
                modeInfos.add(info);
            } else if (this.statusToPrefix.containsKey(c)) {
                String nick = args.removeFirst();
                User user = this.userTracker.getUser(nick);
                ModeInfo info = new ModeInfo(ModeType.STATUS, adding, c, user);
                modeInfos.add(info);
            } else {
                // either ARG_NEVER or unknown mode, in which case assume
                // ARG_NEVER
                ModeInfo info = new ModeInfo(ModeType.ARG_NEVER, adding, c);
                modeInfos.add(info);
            }
        }

        return modeInfos;
    }

    private void processQuit(IRCLine line) {
        String reason = line.getTrailing();

        if (reason == null) {
            reason = "";
        }

        User user = this.userTracker.getUser(line.getNick());
        assert user != null;

        this.userTracker.userQuit(user);

        EventRegistry.getInstance().fire(new QuitEvent(line, user, reason));
    }

    private void processPart(IRCLine line) {
        String reason = line.getTrailing();
        String channelName = line.getParams().get(0);

        Channel channel = this.userTracker.getChannel(channelName);
        User user = this.userTracker.getUser(line.getNick());

        assert channel != null;
        assert user != null;

        if (user.getUsername() == null) {
            user.setUsername(line.getUsername());
            user.setHost(line.getHost());
        } else if (!user.getHost().equals(line.getHost())) {
            user.setHost(line.getHost());
            user.setSynced(false);
            this.userTracker.assignAccount(user);
        }

        this.userTracker.partChannel(channel, user);

        if (user.getNick().equals(this.currentNick)) {
            this.userTracker.untrackChannel(channel);
        }

        EventRegistry.getInstance().fire(new PartEvent(line, channel, user));
    }

    private void processKick(IRCLine line) {
        String channelName = line.getParams().get(0);
        String recipientNick = line.getParams().get(1);
        String reason = line.getTrailing();

        Channel channel = this.userTracker.getChannel(channelName);
        User recipient = this.userTracker.getUser(recipientNick);

        assert channel != null;
        assert recipient != null;

        // kicker can be a server too >:|
        Object kicker;

        if (line.getSenderType() == SenderType.SERVER) {
            kicker = line.getPrefix();
        } else {
            User kickerUser = this.userTracker.getUser(line.getNick());

            // note: chanserv kicks can be from outside the channel
            // -> treat like PM
            // also hidden hosts, again :(

            if (kickerUser == null) {
                kickerUser = this.userTracker.makeUser(line.getNick(),
                        line.getUsername(), line.getHost());
                this.userTracker.trackUser(kickerUser);
            } else if (kickerUser.getUsername() == null) {
                // kick before we receive WHO reply
                kickerUser.setUsername(line.getUsername());
                kickerUser.setHost(line.getHost());
            } else if (!line.getHost().equals(kickerUser.getHost())) {
                assert kickerUser.getUsername().equals(line.getUsername());
                kickerUser.setHost(line.getHost());
                kickerUser.setSynced(false);
                this.userTracker.assignAccount(kickerUser);
            }

            kicker = kickerUser;
        }

        this.userTracker.partChannel(channel, recipient);

        if (recipientNick.equals(this.currentNick)) {
            this.userTracker.untrackChannel(channel);
        }

        EventRegistry.getInstance().fire(new KickEvent(line, channel, recipient, kicker));
    }

    private void processJoin(IRCLine line) {
        String channelName;

        if (line.getTrailing() != null) {
            channelName = line.getTrailing();
        } else {
            channelName = line.getParams().get(0);
        }

        User user = this.userTracker.getUser(line.getNick());

        if (user == null) {
            user = this.userTracker.makeUser(line.getNick(),
                    line.getUsername(), line.getHost());
            this.userTracker.trackUser(user);
        } else {
            if (user.getUsername() == null) {
                user.setUsername(line.getUsername());
                user.setHost(line.getHost());
            } else if (!user.getUsername().equals(line.getUsername())
                    || !user.getHost().equals(line.getHost())) {
                // user parted, other user joins with same nick
                // what about hidden hosts?
                // 1. user joins a channel unidentified (original host)
                // 2. identifies -> hidden host
                // 3. joins another channel

                if (user.getChannels().size() == 0) {
                    this.userTracker.untrackUser(user);
                    user = this.userTracker.makeUser(line.getNick(),
                            line.getUsername(), line.getHost());
                    this.userTracker.trackUser(user);
                } else {
                    assert user.getUsername().equals(line.getUsername());
                    user.setHost(line.getHost());
                    user.setSynced(false);
                    this.userTracker.assignAccount(user);
                }
            }
        }

        Channel channel = this.userTracker.getChannel(channelName);

        if (channel == null) {
            assert line.getNick().equals(this.currentNick);
            channel = this.userTracker.makeChannel(channelName);
            this.userTracker.trackChannel(channel);

            // don't add ourselves to channel user list just yet
            // we are added in NAMREPLY

            if (this.hasWHOX) {
                this.sendQueued(String.format("WHO %s %%uhnar", channelName));
            } else {
                this.sendQueued(String.format("WHO %s", channelName));
            }
            this.sendQueued(String.format("MODE %s", channelName));
        } else {
            assert !line.getNick().equals(this.currentNick);
            assert channel.isSynced();
            this.userTracker.joinChannel(channel, user);

            if (!user.isSynced()) {
                if (this.hasWHOX) {
                    this.sendQueued(String.format("WHO %s %%uhnar",
                            user.getNick()));
                } else {
                    this.sendQueued(String.format("WHO %s", user.getNick()));
                }
            }
        }

        EventRegistry.getInstance().fire(new JoinEvent(line, channel, user));
    }

    private void processNick(IRCLine line) {
        String oldNick = line.getNick();
        String newNick;

        if (line.getTrailing() != null) {
            newNick = line.getTrailing();
        } else {
            newNick = line.getParams().get(0);
        }

        if (oldNick.equals(this.currentNick)) {
            this.currentNick = newNick;
            Bot.logger.info(String.format("We are now known as %s.", newNick));
        }

        User oldUser = this.userTracker.getUser(newNick);

        if (oldUser != null) {
            assert oldUser.getChannels().size() == 0;
            this.userTracker.untrackUser(oldUser);
        }

        User user = this.userTracker.getUser(oldNick);

        if (user == null) {
            assert newNick.equals(this.currentNick) : "Receiving nick change for someone who isn't us";
            user = this.userTracker.makeUser(line.getNick(),
                    line.getUsername(), line.getHost());
            this.userTracker.trackUser(user);
        } else {
            if (!line.getHost().equals(user.getHost())) {
                // new hidden host :'(
                assert line.getUsername().equals(user.getUsername()) : "Impersonation!";
                user.setHost(line.getHost());
                user.setSynced(false);
                this.userTracker.assignAccount(user);
            }
            this.userTracker.changeNick(user, newNick);
        }

        EventRegistry.getInstance().fire(new NickEvent(line, user, oldNick, newNick));
    }
    
    private TargetType getTargetType(String target) {
        if (target.startsWith("$#")) {
            return TargetType.HOSTMASK;
        } else if (target.startsWith("$")) {
            return TargetType.SERVERMASK;
        } else if (this.prefixToStatus.containsKey(target.charAt(0))
                && this.chanTypes.indexOf(target.charAt(1)) != -1) {
            return TargetType.STATUSMSG;
        } else if (this.chanTypes.indexOf(target.charAt(0)) != -1) {
            return TargetType.CHANNEL;
        } else {
            return TargetType.NICK;
        }
    }

    private void processNotice(IRCLine line) {
        // things to take into account
        // - target can be either #channel, @#statusmsg, ourNick
        // - no such thing as ROLEPLAY notices
        // - we might not have received WHO data for the user yet:
        //   username/host is None
        // - for PMs, user@host might be different if a different user talks to us
        //   -> untrack old, track new user
        // - messages without join (-n) and PMs should be treated similarly
        //   (create/track new user if user is unknown, untrack old, track new user
        //   if user@host is different and they share no channels with us)
        // - a channel user might have a different host if they joined the channel
        //   unidentified then identified, being assigned a new hidden host
        //   -> update host of user object, desync it
         

        if (line.getSenderType() == SenderType.SERVER) {
            // ignore server notices
            return;
        }

        String target = line.getParams().get(0);
        String message = line.getTrailing();
        TargetType targetType = this.getTargetType(target);

        Channel channel = null;

        if (targetType == TargetType.CHANNEL || targetType == TargetType.STATUSMSG) {
            String channelName;

            if (targetType == TargetType.CHANNEL) {
                channelName = target;
            } else {
                channelName = target.substring(1);
            }

            channel = this.userTracker.getChannel(channelName);
            assert channel != null;
        }

        User user = this.userTracker.getUser(line.getNick());

        // no such thing as roleplay notices
        if (user == null) {
            // private notice or message without join
            user = this.userTracker.makeUser(line.getNick(),
                    line.getUsername(), line.getHost());
            this.userTracker.trackUser(user);
        } else if (user.getUsername() == null) {
            user.setUsername(line.getUsername());
            user.setHost(line.getHost());
        } else if (!user.getUsername().equals(line.getUsername())
                || !user.getHost().equals(line.getHost())) {
            // PM or new hidden host message
            if (user.getChannels().size() == 0) {
                // PM
                this.userTracker.untrackUser(user);
                user = this.userTracker.makeUser(line.getNick(),
                        line.getUsername(), line.getHost());
                this.userTracker.trackUser(user);
            } else {
                // new hidden host
                assert user.getUsername().equals(line.getUsername()) : "Impersonation!";
                user.setHost(line.getHost());
                user.setSynced(false);
                this.userTracker.assignAccount(user);
            }
        }
        
        if (message.startsWith("\01") && message.endsWith("\01")) {
            // CTCP message

            String cmd;
            String inp;
            
            int space = message.indexOf(" ", 1);
            
            if (space == -1) {
                cmd = message.substring(1, message.length()-1).toUpperCase();
                inp = "";
            } else {
                cmd = message.substring(1, space).toUpperCase();
                inp = message.substring(space + 1, message.length() - 1);
            }
            
            EventRegistry.getInstance().fire(new CTCPResponseEvent(line, target, targetType, cmd, inp, user, channel));
        } else {
            EventRegistry.getInstance().fire(new NoticeEvent(line, target, targetType, message, user, channel));
        }
    }

    private void processPrivmsg(IRCLine line) {
        // things to take into account
        // - target can be either #channel, @#statusmsg, ourNick
        // - ROLEPLAY might be enabled on the server: host = npc.fakeuser.invalid
        //   -> create temporary untracked user object
        // - we might not have received WHO data for the user yet:
        //   username/host is None
        // - for PMs, user@host might be different if a different user talks to us
        //   -> untrack old, track new user
        // - messages without join (-n) and PMs should be treated similarly
        //   (create/track new user if user is unknown, untrack old, track new user
        //   if user@host is different and they share no channels with us)
        // - a channel user might have a different host if they joined the channel
        //   unidentified then identified, being assigned a new hidden host -> update
        //   host of user object, desync it

        assert line.getSenderType() == SenderType.USER;

        String target = line.getParam(0);
        String message = line.getTrailing();

        Channel channel = null;

        TargetType targetType = this.getTargetType(target);

        if (targetType == TargetType.CHANNEL || targetType == TargetType.STATUSMSG) {
            String channelName;

            if (targetType == TargetType.CHANNEL) {
                channelName = target;
            } else {
                channelName = target.substring(1);
            }

            channel = this.userTracker.getChannel(channelName);
            assert channel != null;
        }

        User user;

        if (line.getHost().equals("npc.fakuser.invalid")) {
            user = this.userTracker.makeUser(line.getNick(),
                    line.getUsername(), line.getHost());
        } else {
            user = this.userTracker.getUser(line.getNick());

            // user is null -> message without join or PM
            // host is different -> new hidden host (update user object)

            if (user == null) {
                // message without join or PM
                user = this.userTracker.makeUser(line.getNick(),
                        line.getUsername(), line.getHost());
                this.userTracker.trackUser(user);
            } else if (user.getUsername() == null) {
                // channel message before we receive a WHO reply
                // -> update user@host
                user.setUsername(line.getUsername());
                user.setHost(line.getHost());
            } else if (!user.getUsername().equals(line.getUsername())
                    || !user.getHost().equals(line.getHost())) {
                // PM or new hidden host message
                if (user.getChannels().size() == 0) {
                    // PM
                    this.userTracker.untrackUser(user);
                    user = this.userTracker.makeUser(line.getNick(),
                            line.getUsername(), line.getHost());
                    this.userTracker.trackUser(user);
                } else {
                    // new hidden host
                    assert user.getUsername().equals(line.getUsername()) : "Impersonation!";
                    user.setHost(line.getHost());
                    user.setSynced(false);
                    this.userTracker.assignAccount(user);
                }
            }
        }

        if (message.startsWith("\01") && message.endsWith("\01")) {
            // CTCP message

            String cmd;
            String inp;
            
            int space = message.indexOf(" ", 1);
            
            if (space == -1) {
                cmd = message.substring(1, message.length()-1).toUpperCase();
                inp = "";
            } else {
                cmd = message.substring(1, space).toUpperCase();
                inp = message.substring(space + 1, message.length() - 1);
            }

            if (cmd.equals("ACTION")) {
                EventRegistry.getInstance().fire(new ActionEvent(line, target, targetType, cmd, inp, user, channel));
            } else {
                if (cmd.equals("TIME")) {
                    this.sendCTCPResponse(user, "TIME", new Date().toString());
                } else if (cmd.equals("PING")) {
                    this.sendCTCPResponse(user, "PING", inp);
                } else if (this.bot.getConfig().getCtcpReplies()
                        .containsKey(cmd)) {
                    this.sendCTCPResponse(user, cmd, this.bot.getConfig()
                            .getCtcpReplies().get(cmd));
                }
                EventRegistry.getInstance().fire(new CTCPEvent(line, target, targetType, cmd, inp, user, channel));
            }
        } else {
            EventRegistry.getInstance().fire(new MessageEvent(line, target, targetType, message, user, channel));
        }
    }

    private void processENDOFMOTD(IRCLine line) {
        String pass = this.bot.getConfig().getConnectionInfo()
                .getNickservPassword();

        if (pass != null && !pass.isEmpty()) {
            sendMessage("NickServ", String.format("IDENTIFY %s", pass));
        }

        for (String channelName : this.bot.getConfig().getChannels()) {
            this.sendQueued(String.format("JOIN %s", channelName));
            this.sendMessage(channelName, "herp");
        }
    }

    private void processISUPPORT(IRCLine line) {
        for (int i = 1; i < line.getParams().size(); ++i) {
            String param = line.getParams().get(i);

            if (param.startsWith("CHANTYPES=")) {
                this.chanTypes = param.substring("CHANTYPES=".length());
            } else if (param.startsWith("CHANMODES=")) {
                this.chanModes = param.substring("CHANMODES=".length()).split(
                        ",");
            } else if (param.startsWith("PREFIX=")) {
                this.prefixToStatus.clear();
                this.statusToPrefix.clear();
                String modeChars = param.substring("PREFIX=".length());
                int rparen = modeChars.indexOf(")");
                String modes = modeChars.substring(1, rparen);
                String chars = modeChars.substring(rparen + 1);
                for (int j = 0; j < modes.length(); ++j) {
                    char mode = modes.charAt(j);
                    char prefix = chars.charAt(j);
                    this.statusToPrefix.put(mode, prefix);
                    this.prefixToStatus.put(prefix, mode);
                }
            } else if (param.equals("WHOX")) {
                this.hasWHOX = true;
            }
        }
    }

    public void sendQueued(String line) {
        this.sendQueue.add(line);
    }

    public void sendNow(String line) {
        if (!line.endsWith("\r\n")) {
            line += "\r\n";
        }
        
        try {
            out.write(line.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Bot.logger.warning(String.format(
                    "Not sending line %s, cannot encode to UTF-8", line));
        } catch (IOException e) {
            Bot.logger.warning(String.format("Cannot send line %s:", line));
            Bot.logger.throwing("Connection", "sendNow", e);
        }
    }

    public void sendMessage(String target, String message) {
        if (message.isEmpty()) {
            return;
        }

        String[] lines = message.split("\n");

        for (String line : lines) {
            this.sendQueued(String.format("PRIVMSG %s :%s", target, line));
        }
    }

    public void sendMessage(Channel channel, String message) {
        this.sendMessage(channel.getName(), message);
    }

    public void sendMessage(User user, String message) {
        this.sendMessage(user.getNick(), message);
    }

    public void sendNotice(String target, String message) {
        if (message.isEmpty()) {
            return;
        }

        String[] lines = message.split("\n");

        for (String line : lines) {
            this.sendQueued(String.format("NOTICE %s :%s", target, line));
        }
    }

    public void sendNotice(Channel channel, String message) {
        this.sendNotice(channel.getName(), message);
    }

    public void sendNotice(User user, String message) {
        this.sendNotice(user.getNick(), message);
    }

    public void sendCTCPResponse(String target, String command, String message) {
        StringBuilder msg = new StringBuilder();
        msg.append('\01');
        msg.append(command);
        if (!message.isEmpty()) {
            msg.append(' ');
            msg.append(message);
        }
        msg.append('\01');
        
        this.sendNotice(target, msg.toString());
    }

    public void sendCTCPResponse(Channel channel, String command, String message) {
        this.sendCTCPResponse(channel.getName(), command, message);
    }

    public void sendCTCPResponse(User user, String command, String message) {
        this.sendCTCPResponse(user.getNick(), command, message);
    }
    
    public void sendCTCPMessage(String target, String command, String message) {
        StringBuilder msg = new StringBuilder();
        msg.append('\01');
        msg.append(command);
        if (!message.isEmpty()) {
            msg.append(' ');
            msg.append(message);
        }
        msg.append('\01');
        
        this.sendMessage(target, msg.toString());
    }
    
    public void sendCTCPMessage(User user, String command, String message) {
        sendCTCPMessage(user.getNick(), command, message);
    }

    public void sendCTCPMessage(Channel channel, String command, String message) {
        sendCTCPMessage(channel.getName(), command, message);
    }
    
    public void sendAction(String target, String message) {
        this.sendCTCPMessage(target, "ACTION", message);
    }

    public void sendAction(Channel channel, String message) {
        this.sendAction(channel.getName(), message);
    }

    public void sendAction(User user, String message) {
        this.sendAction(user.getNick(), message);
    }
    
    public void quit(String reason) {
        this.sendNow(String.format("QUIT :%s", reason));
    }
    
    public boolean isOnChannel(String channelName) {
        return this.getUserTracker().getChannel(channelName) != null;
    }
}
