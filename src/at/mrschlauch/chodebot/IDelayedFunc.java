package at.mrschlauch.chodebot;

public interface IDelayedFunc {
    /**
     * @return true to reschedule at the current delay, false to stop
     *         rescheduling
     */
    public boolean call();
}
