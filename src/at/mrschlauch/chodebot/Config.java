package at.mrschlauch.chodebot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import com.google.gson.annotations.SerializedName;

import lombok.Getter;

@Getter
public class Config {
    @SerializedName("irc")
    private ConnectionInfo connectionInfo = new ConnectionInfo();
    private Set<Account> accounts = new HashSet<Account>();
    private Set<String> channels = new HashSet<String>();
    private Set<String> disabledPlugins = new HashSet<String>();
    private Map<String, String> ctcpReplies = new HashMap<String, String>();
    private long reconnectionInterval = 30*1000;

    public Config() {
        connectionInfo.setNick("ChodeBot[]");
        connectionInfo.setUsername("lorbo");
        connectionInfo.setRealname("_303's newest abomination");
        connectionInfo.setServer("irc.rozznet.net");
        connectionInfo.setPort(6667);
        channels.add("#bot");
        ctcpReplies.put("VERSION", "jChodeBot v0.1");
        ctcpReplies.put("FINGER",
                "Not without drinks and dinner first you won't.");
    }

    public boolean addAccount(String handle, boolean admin) {
        if (this.hasAccount(handle)) {
            Bot.logger.warning(String.format(
                    "Trying to add account %s which already exists.", handle));
            return false;
        }
        Account account = new Account(handle, admin);
        this.accounts.add(account);
        return true;
    }

    public boolean deleteAccount(String handle) {
        if (!this.hasAccount(handle)) {
            Bot.logger
                    .warning(String.format(
                            "Trying to delete account %s which doesn't exist.",
                            handle));
            return false;
        }
        Account acc = this.getAccount(handle);
        this.accounts.remove(acc);
        return true;
    }

    public boolean hasAccount(String handle) {
        return this.getAccount(handle) != null;
    }

    public Account getAccount(String handle) {
        for (Account account : this.accounts) {
            if (account.getHandle().equals(handle)) {
                return account;
            }
        }

        return null;
    }
}
