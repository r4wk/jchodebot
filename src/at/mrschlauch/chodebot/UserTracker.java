package at.mrschlauch.chodebot;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class UserTracker {
    private Map<String, User> users = new HashMap<String, User>();
    private Map<String, Channel> channels = new HashMap<String, Channel>();
    private Connection connection;

    public UserTracker(Connection conn) {
        this.connection = conn;
    }

    public Collection<Channel> getChannels() {
        return this.channels.values();
    }

    public User makeUser(String nick, String username, String host) {
        return new User(this.connection, Utils.toIRCLowercase(nick), username, host);
    }

    public User makeUser(String nick) {
        return new User(this.connection, Utils.toIRCLowercase(nick));
    }

    public User getUser(String nick) {
        return this.users.get(Utils.toIRCLowercase(nick));
    }

    public void trackUser(User user) {
        assert !this.users.containsKey(user.getNick()) : "Already tracking user!";
        this.users.put(user.getNick(), user);
    }

    public void untrackUser(User user) {
        assert this.users.containsKey(user.getNick()) : "Not tracking user!";
        this.users.remove(user.getNick());
    }

    public void changeNick(User user, String newNick) {
        // make sure you manually untrack outdated users with nick newNick
        assert this.users.containsKey(user.getNick()) : "Not tracking user!";
        assert !this.users.containsKey(newNick) : "Already tracking user!";

        this.users.put(newNick, user);
        this.users.remove(user.getNick());
        user.setNick(newNick);
    }

    public Channel makeChannel(String name) {
        return new Channel(this.connection, Utils.toIRCLowercase(name));
    }

    public Channel getChannel(String channelName) {
        return this.channels.get(Utils.toIRCLowercase(channelName));
    }

    public void trackChannel(Channel channel) {
        assert !this.channels.containsKey(channel.getName()) : "Already tracking channel!";
        this.channels.put(channel.getName(), channel);
    }

    public void untrackChannel(Channel channel) {
        assert this.channels.containsKey(channel.getName()) : "Not tracking channel!";
        this.channels.remove(channel.getName());

        for (User user : channel.getUsers()) {
            user.removeChannel(channel);
        }
    }

    public void joinChannel(Channel channel, User user) {
        this.joinChannel(channel, user, "");
    }

    public void joinChannel(Channel channel, User user, String statuses) {
        channel.addUser(user, statuses);
        user.addChannel(channel);
    }

    public void partChannel(Channel channel, User user) {
        channel.removeUser(user);
        user.removeChannel(channel);
    }

    public void userQuit(User user) {
        for (Channel channel : user.getChannels()) {
            channel.removeUser(user);
        }

        this.untrackUser(user);
    }

    public void assignAccount(User user) {
        Account account;

        if (!user.isSynced()) {
            account = null;
        } else {
            account = this.connection.getBot().getConfig()
                    .getAccount(user.getNickservAccount());
        }

        user.setAccount(account);
    }

    public void invalidateAccounts() {
        long now = System.currentTimeMillis();

        for (User user : this.users.values()) {
            if (user.isSynced() && now - user.getSyncTime() > 10 * 60 * 1000) {
                user.setSynced(false);
                user.setAccount(null);
            }
        }
    }
}
