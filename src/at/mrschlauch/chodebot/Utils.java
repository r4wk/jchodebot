package at.mrschlauch.chodebot;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final Pattern interpolatePattern = Pattern.compile("\\$\\{([^}]+)\\}|\\$(\\w+)");
    
    public static String decode(byte[] buf) {
        return decode(buf, 0, buf.length);
    }

    public static String[] encodings = new String[] { "UTF-8", "ISO-8859-1" };

    public static String decode(byte[] buf, int offset, int length) {
        for (String encoding : encodings) {
            try {
                return new String(buf, offset, length, encoding);
            } catch (UnsupportedEncodingException e) {
            }
        }

        return new String(buf, offset, length);
    }

    public static int lastIndexOf(byte[] buf, byte key) {
        for (int i = buf.length - 1; i >= 0; --i) {
            if (buf[i] == key) {
                return i;
            }
        }

        return -1;
    }

    public static String toIRCLowercase(String s) {
        s = s.toLowerCase();
        s = s.replaceAll("\\[", "{");
        s = s.replaceAll("\\]", "}");
        s = s.replaceAll("\\\\", "|");
        s = s.replaceAll("\\^", "~");
        return s;
    }
    
    public static boolean equalsIgnoreIRCCase(String s1, String s2) {
        return toIRCLowercase(s1).equals(toIRCLowercase(s2));
    }

    public static String join(String glue, String[] parts) {
        return join(glue, Arrays.asList(parts));
    }

    public static String join(String glue, Collection<String> parts) {
        StringBuilder sb = new StringBuilder();

        boolean first = true;

        for (String part : parts) {
            if (first) {
                first = false;
            } else {
                sb.append(glue);
            }
            sb.append(part);
        }

        return sb.toString();
    }
    
    public static String center(String s, int size) {
        return center(s, size, " ");
    }
    
    public static String center(String s, int size, String pad) {
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < (size-s.length())/2; ++i) {
            sb.append(pad);
        }
        
        sb.append(s);
        
        while (sb.length() < size) {
            sb.append(pad);
        }
        
        return sb.toString();
    }
    
    public static boolean matchesMask(String mask, String s) {
        s = toIRCLowercase(s);
        mask = toIRCLowercase(s);
        mask = mask.replaceAll("\\.", "\\.");
        mask = mask.replaceAll("\\$", "\\$");
        mask = mask.replaceAll("\\(", "\\(");
        mask = mask.replaceAll("\\)", "\\)");
        mask = mask.replaceAll("\\{", "\\{");
        mask = mask.replaceAll("\\}", "\\}");
        mask = mask.replaceAll("\\+", "\\+");
        mask = mask.replaceAll("\\|", "\\|");
        mask = mask.replaceAll("\\?", ".");
        mask = mask.replaceAll("\\*", ".*");
        return Pattern.compile(mask).matcher(s).matches();
    }
    
    public static String interpolate(String format, Map<String,String> variables) {
        StringBuilder sb = new StringBuilder();
        Matcher m = interpolatePattern.matcher(format);
        int previousEnd = 0;
        
        while (m.find()) {
            sb.append(format.substring(previousEnd, m.start()));
            String key = m.group(1) != null ? m.group(1) : m.group(2);
            String val = variables.get(key);
            
            if (val == null) {
                val = m.group(0);
            }
            
            sb.append(val);
            previousEnd = m.end();
        }
        sb.append(format.substring(previousEnd, format.length()));
        
        return sb.toString();
    }
}
