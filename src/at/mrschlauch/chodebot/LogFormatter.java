package at.mrschlauch.chodebot;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {
    public LogFormatter() {}

    @Override
    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder();
        Date date = new Date(record.getMillis());
        builder.append(date.toString());
        builder.append(' ');
        builder.append(record.getLevel().getName());
        builder.append(' ');
        builder.append(formatMessage(record));
        builder.append("\n");
        
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            record.getThrown().printStackTrace(pw);
            pw.close();
            builder.append(sw.toString());
        }
        builder.append("\n");
        
        return builder.toString();
    }
}
