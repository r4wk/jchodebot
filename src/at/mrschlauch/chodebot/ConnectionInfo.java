package at.mrschlauch.chodebot;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConnectionInfo {
    private String nick;
    private String username;
    private String realname;
    private String nickservPassword;
    private String serverPassword;
    private String server;
    private int port;
}
