package at.mrschlauch.chodebot.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.toposort.CyclicDependencyError;
import at.mrschlauch.chodebot.toposort.TopoSorter;

public class PluginLoader {
	private static File pluginDir = new File("plugins");
	private Bot bot;
	// private List<File> jars = new ArrayList<File>();
	private List<File> directories = new ArrayList<File>();
	private List<PluginContainer> pluginContainers = new ArrayList<PluginContainer>();
	private URLClassLoader classLoader;
	
	public PluginLoader(Bot bot) {
		this.bot = bot;
	}
	
	public Object getPlugin(String name) {
	    for (PluginContainer pc: this.pluginContainers) {
	        if (pc.getName().equals(name)) {
	            return pc.getPlugin();
	        }
	    }
	    
	    return null;
	}
	
	public void loadPlugins() {
		populateClassPath();
		discoverPlugins();
		prepareDependencies();
		
		try {
			sortPlugins();
		} catch (CyclicDependencyError e) {
			Bot.logger.severe("Cyclic plugin dependencies detected. Aborting loading.");
			return;
		}
		
		initPlugins();
		postInitPlugins();
	}

	private void populateClassPath() {
		List<URL> urls = new ArrayList<URL>();
		
	    for (File f: pluginDir.listFiles()) {
	    	if (f.isFile() && f.getName().endsWith(".jar")) {
	    		// TODO: jar plugin loading
	    	} else if (f.isDirectory()) {
	    		Bot.logger.info(String.format("Found plugin directory %s", f));
    			try {
					urls.add(f.toURI().toURL());
				} catch (MalformedURLException e) {
					Bot.logger.severe(String.format("Invalid plugin directory path %s. Not loading.", f));
				}
	    		this.directories.add(f);
	    	}
	    }
	    
	    URL[] urlArray = urls.toArray(new URL[urls.size()]);
	    this.classLoader = new URLClassLoader(urlArray);
	}
	
	private void discoverPlugins() {
		// TODO: jar plugin loading
		for (File dir: this.directories) {
			IDiscoverer discoverer = new DirectoryDiscoverer(this.classLoader, dir);
			pluginContainers.addAll(discoverer.discover());
		}
	}
	
	private void prepareDependencies() {
		Map<String,PluginContainer> pcMap = new HashMap<String,PluginContainer>();
		
		for (PluginContainer pc: this.pluginContainers) {
			pcMap.put(pc.getName(), pc);
		}
		
		pcs:
		for (PluginContainer pc: this.pluginContainers) {
			for (String depName: pc.getDependencyNames()) {
				PluginContainer dep = pcMap.get(depName);
				
				if (dep == null) {
					Bot.logger.severe(String.format("Plugin %s is missing dependency %s. Disabling it.", pc.getName(), depName));
					pc.setDisabled(true);
					continue pcs;
				}
				
				if (dep.isDisabled()) {
					Bot.logger.warning(String.format("Disabling plugin %s since one of its dependencies is.", pc.getName()));
					pc.setDisabled(true);
				}
				
				pc.getDependencies().add(dep);
				dep.getDependants().add(pc);
			}
			
			if (this.bot.getConfig().getDisabledPlugins().contains(pc.getName())) {
				pc.setDisabled(true);
			}
		}
	}

	private void sortPlugins() throws CyclicDependencyError {
		TopoSorter<PluginContainer> sorter = new TopoSorter<PluginContainer>();
		pluginContainers = sorter.sort(pluginContainers);
	}
	
	private void initPlugins() {
		for (PluginContainer pc: this.pluginContainers) {
			if (pc.isDisabled()) {
				Bot.logger.warning(String.format("Skipping inititalizing plugin %s because it is disabled.", pc.getName()));
				continue;
			}
			
			try {
				pc.init();
			} catch (Exception e) {
				Bot.logger.severe(String.format("Failed to initialize plugin %s. Disabling it and its dependants.", pc.getName()));
				Bot.logger.throwing("PluginLoader", "initPlugins", e);
				pc.setDisabled(true);
				for (PluginContainer dependant: pc.getDependants()) {
					Bot.logger.warning(String.format("Disabling plugin %s since one of its dependencies is.", dependant.getName()));
					dependant.setDisabled(true);
				}
				continue;
			}
		}
	}
	
	private void postInitPlugins() {
		for (PluginContainer pc: this.pluginContainers) {
			if (pc.isDisabled()) {
				Bot.logger.warning(String.format("Skipping post-inititalizing plugin %s because it is disabled.", pc.getName()));
				continue;
			}
			
			try {
				pc.postInit();
			} catch (Exception e) {
				Bot.logger.severe(String.format("Failed to post-initialize plugin %s. Disabling it and its dependants.", pc.getName()));
				Bot.logger.throwing("PluginLoader", "postInitPlugins", e);
				pc.setDisabled(true);
				for (PluginContainer dependant: pc.getDependants()) {
					Bot.logger.warning(String.format("Disabling plugin %s since one of its dependencies is.", dependant.getName()));
					dependant.setDisabled(true);
				}
				continue;
			}
		}
	}
}
