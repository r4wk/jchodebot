package at.mrschlauch.chodebot.plugins;

import java.io.File;
import java.io.FileFilter;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.mrschlauch.chodebot.Bot;

public class DirectoryDiscoverer implements IDiscoverer {
	// ^$ so we don't match inner/nested classes
	private static Pattern classPattern = Pattern.compile("([^\\s$]+)\\.class");
	private URLClassLoader classLoader;
	private File baseDir;
	
    private static class ClassFilter implements FileFilter
    {
        @Override
        public boolean accept(File file)
        {
        	// either .class files or directories
            return (file.isFile() && classPattern.matcher(file.getName()).matches()) || file.isDirectory();
        }
    }
	
	public DirectoryDiscoverer(URLClassLoader classLoader, File baseDir) {
		this.classLoader = classLoader;
		this.baseDir = baseDir;
	}
	
	@Override
	public List<PluginContainer> discover() {
		List<PluginContainer> foundPlugins = new ArrayList<PluginContainer>();
		
		discoverRecursively("", this.baseDir, foundPlugins);
		
		return foundPlugins;
	}
	
	private void discoverRecursively(String path, File dir, List<PluginContainer> foundPlugins) {
		for (File file: dir.listFiles(new ClassFilter())) {
			if (file.isDirectory()) {
				String newPath = path.isEmpty() ? file.getName() : path+"."+file.getName();
				//Bot.logger.info(String.format("Recursing into package %s in plugin directory %s", newPath, this.baseDir));
				discoverRecursively(newPath, file, foundPlugins);
				continue;
			}
			
			Matcher matcher = classPattern.matcher(file.getName());
			
			if (!matcher.matches()) {
				continue; // impossible
			}
			
			String className = matcher.group(1);
			String qualifiedName = path.isEmpty() ? file.getName() : path+"."+className;
			
			Class<?> theClass;
			try {
				theClass = this.classLoader.loadClass(qualifiedName);
			} catch (ClassNotFoundException e) {
				Bot.logger.severe(String.format("Could not load plugin candidate %s in directory %s: %s", qualifiedName, this.baseDir, e.getMessage()));
				Bot.logger.throwing("DirectoryDiscoverer", "discoverRecursively", e);
				continue;
			}
			
			Plugin ann = theClass.getAnnotation(Plugin.class);
			
			if (ann == null) {
				continue;
			}
			
			Object plugin;
			try {
				plugin = theClass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				Bot.logger.warning(String.format("Could not instantiate plugin class %s in directory %s", qualifiedName, this.baseDir));
				continue;
			}
			
			PluginContainer pc = new PluginContainer(plugin, ann);
			foundPlugins.add(pc);
			Bot.logger.info(String.format("Found plugin %s in directory %s", qualifiedName, this.baseDir));
		}
	}
}