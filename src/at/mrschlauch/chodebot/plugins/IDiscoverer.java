package at.mrschlauch.chodebot.plugins;

import java.util.List;

public interface IDiscoverer {
	public List<PluginContainer> discover();
}