package at.mrschlauch.chodebot.plugins;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import at.mrschlauch.chodebot.Bot;

public class PluginUtils {
    private static final File configDir = new File("config");
    private static final String fileNameFormat = "%s.json";
    
    public static <T> T loadOrCreateConfig(String pluginName, Class<T> cls, T defaultConfig) {
	    T newConfig = loadConfig(pluginName, cls);
	    
	    if (newConfig != null) {
	        return newConfig;
	    }
	    
        PluginUtils.saveConfig(pluginName, defaultConfig);
        return defaultConfig;
    }
    
    public static <T> T loadConfig(String pluginName, Class<T> cls) {
        File configPath = new File(configDir, String.format(fileNameFormat, pluginName));
        
        if (!configPath.exists()) {
            Bot.logger.severe(String.format("Failed to load plugin config for plugin %s: config file does not exist.", pluginName));
            return null;
        }
        
        Reader in = null;
        
        try {
            in = new FileReader(configPath);
        } catch (IOException e) {
            Bot.logger.severe(String.format("Failed to load plugin config for plugin %s.", pluginName));
            Bot.logger.throwing("PluginUtils", "loadConfig", e);
            return null;
        }

        Gson gson = new Gson();
        T config;

        try {
            config = gson.fromJson(in, cls);
        } catch (JsonIOException e) {
            Bot.logger.severe(String.format("Failed to load plugin config for plugin %s.", pluginName));
            Bot.logger.throwing("PluginUtils", "loadConfig", e);
            return null;
        } catch (JsonSyntaxException e) {
            Bot.logger.severe(String.format("Failed to load plugin config for plugin %s: Syntax error.", pluginName));
            Bot.logger.throwing("PluginUtils", "loadConfig", e);
            return null;
        } finally {
            try { in.close(); } catch (IOException e) {}
        }
        
        Bot.logger.info(String.format("Loaded plugin config for plugin %s.", pluginName));
        
        return config;
    }
    
    public static void saveConfig(String pluginName, Object config) {
        if (!configDir.exists()) {
            if (!configDir.mkdir()) {
                Bot.logger.severe(String.format("Failed to save plugin config for plugin %s: config directory could not be created", pluginName));
            }
        } else if (!configDir.isDirectory()) {
            Bot.logger.severe(String.format("Failed to save plugin config for plugin %s: config directory is not a directory.", pluginName));
            return;
        }
        
        File configPath = new File(configDir, String.format(fileNameFormat, pluginName));
        
        FileWriter out = null;

        try {
            out = new FileWriter(configPath);
        } catch (IOException e) {
            Bot.logger.severe(String.format("Failed to save plugin config file for plugin %s.", pluginName));
            Bot.logger.throwing("PluginUtils", "saveConfig", e);
            return;
        }
        
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

        try {
            gson.toJson(config, out);
        } catch (JsonIOException e) {
            Bot.logger.severe(String.format("Failed to save plugin config file for plugin %s.", pluginName));
            Bot.logger.throwing("PluginUtils", "saveConfig", e);
            return;
        } finally {
            try { out.close(); } catch (IOException e) {}
        }
        
        Bot.logger.info(String.format("Saved plugin config file for plugin %s.", pluginName));
    }
}
