package at.mrschlauch.chodebot.plugins;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import at.mrschlauch.chodebot.toposort.IDependant;

import lombok.Getter;
import lombok.Setter;

@Getter
public class PluginContainer implements IDependant<PluginContainer> {
	private Object plugin;
	private String name;
	private String description;
	private String[] dependencyNames;
	@Setter private boolean disabled;
	private boolean initialized;
	private boolean postInitialized;
	private Set<PluginContainer> dependencies = new HashSet<PluginContainer>();
	private Set<PluginContainer> dependants = new HashSet<PluginContainer>();
	
	public PluginContainer(Object plugin, Plugin annotation) {
		this.plugin = plugin;
		this.name = annotation.name();
		this.description = annotation.description();
		this.dependencyNames = annotation.dependencies();
	}
	
	public <T extends Annotation> Map<Method,T> getMethods(Class<T> annotationClass) {
		Map<Method,T> methods = new HashMap<Method,T>();
		
		for (Method m: this.plugin.getClass().getDeclaredMethods()) {
			T annotation = m.getAnnotation(annotationClass);
			if (annotation != null) {
				methods.put(m, annotation);
			}
		}
		
		return methods;
	}
	
	public <T extends Annotation> Map<Method,T> getMethods(Class<T> annotationClass, Class<?>[] paramTypes) {
		Map<Method,T> methods = new HashMap<Method,T>();
		
		for (Method m: this.plugin.getClass().getDeclaredMethods()) {
			T annotation = m.getAnnotation(annotationClass);
			if (annotation != null && Arrays.equals(m.getParameterTypes(), paramTypes)) {
				methods.put(m, annotation);
			}
		}
		
		return methods;
	}
	
	public <T extends Annotation> void invokeMethods(Class<T> annotationClass, Class<?>[] paramTypes, Object[] args) throws Exception {
		Map<Method,T> methods = this.getMethods(annotationClass, paramTypes);
		
		for (Method m: methods.keySet()) {
			m.invoke(this.plugin, args);
		}
	}
	
	public void init() throws Exception {
		this.invokeMethods(Init.class, new Class<?>[0], new Object[0]);
		this.initialized = true;
	}
	
	public void postInit() throws Exception {
		this.invokeMethods(PostInit.class, new Class<?>[0], new Object[0]);
		this.postInitialized = true;
	}
}
