package at.mrschlauch.chodebot;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account {
    private String handle;
    private boolean admin;

    public Account() {
    }

    public Account(String handle, boolean admin) {
        this.handle = handle;
        this.admin = admin;
    }
}
