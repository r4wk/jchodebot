package at.mrschlauch.chodebot;

public class Colors {
    public static String NORMAL = "\u000f";
    public static String BOLD = "\u0002";
    public static String UNDERLINE = "\u001f";
    public static String REVERSE = "\u0016";
    public static String WHITE = "\u000300";
    public static String BLACK = "\u000301";
    public static String DARK_BLUE = "\u000302";
    public static String DARK_GREEN = "\u000303";
    public static String RED = "\u000304";
    public static String BROWN = "\u000305";
    public static String PURPLE = "\u000306";
    public static String OLIVE = "\u000307";
    public static String YELLOW = "\u000308";
    public static String GREEN = "\u000309";
    public static String TEAL = "\u000310";
    public static String CYAN = "\u000311";
    public static String BLUE = "\u000312";
    public static String MAGENTA = "\u000313";
    public static String DARK_GRAY = "\u000314";
    public static String LIGHT_GRAY = "\u000315";
    
    private Colors() {}
}
