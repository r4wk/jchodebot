package at.mrschlauch.chodebot;

public enum TargetType {
    NICK,
    CHANNEL,
    STATUSMSG,
    SERVERMASK,
    HOSTMASK;
}
