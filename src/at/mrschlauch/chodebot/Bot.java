package at.mrschlauch.chodebot;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import at.mrschlauch.chodebot.plugins.PluginLoader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import lombok.Getter;

@Getter
public class Bot {
    public static Logger logger = Logger.getLogger("ChodeBot");
    static {
        try {
            FileHandler fh = new FileHandler("bot.log");
            logger.addHandler(fh);  
            logger.setLevel(Level.ALL);  
            fh.setFormatter(new LogFormatter());  
            logger.addHandler(fh);
        } catch (SecurityException | IOException e) {
            logger.severe("Failed to register log file handler.");
            e.printStackTrace();
        }
    }
    private static Bot instance;
    private File configPath;
    private Config config;
    private Connection connection;
    private PluginLoader pluginLoader;

    public Bot(File configPath) {
        instance = this;
        this.configPath = configPath;
        this.loadConfig();
        this.pluginLoader = new PluginLoader(this);
        this.pluginLoader.loadPlugins();
    }

    public static Bot getInstance() {
        return instance;
    }

    public void loadConfig() {
        if (!this.configPath.exists()) {
            Bot.logger.warning("Config file doesn't exist. Saving defaults and exiting.");
            this.config = new Config();
            this.saveConfig();
            System.exit(1);
        }

        Reader in = null;
        try {
            in = new FileReader(this.configPath);
        } catch (IOException e) {
            Bot.logger.severe("Failed to load config file. Exiting.");
            Bot.logger.throwing("Bot", "loadConfig", e);
            System.exit(1);
        }

        Gson gson = new Gson();

        try {
            this.config = gson.fromJson(in, Config.class);
        } catch (JsonIOException e) {
            Bot.logger.severe("Failed to load config file. Exiting.");
            Bot.logger.throwing("Bot", "loadConfig", e);
            System.exit(1);
        } catch (JsonSyntaxException e) {
            Bot.logger.severe("Syntax error in config file. Exiting.");
            Bot.logger.throwing("Bot", "loadConfig", e);
            System.exit(1);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }

        Bot.logger.info("Loaded config file.");
    }

    public void saveConfig() {
        FileWriter out = null;

        try {
            out = new FileWriter(this.configPath);
        } catch (IOException e) {
            Bot.logger.severe("Failed to save config file. Exiting.");
            Bot.logger.throwing("Bot", "saveConfig", e);
            System.exit(1);
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

        try {
            gson.toJson(this.config, out);
        } catch (JsonIOException e) {
            Bot.logger.severe("Failed to save config file. Exiting.");
            Bot.logger.throwing("Bot", "saveConfig", e);
            System.exit(1);
        } finally {
            try { out.close(); } catch (IOException e) {}
        }
        Bot.logger.info("Saved config file.");
    }

    public void connect() {
        if (this.connection != null) {
            logger.severe("Trying to connect while already connected!");
            return;
        }

        while (true) {
            this.connection = new Connection(this);
            this.connection.connect();
            
            if (this.connection.isReconnecting()) {
                try {
                    Thread.sleep(this.config.getReconnectionInterval());
                } catch (InterruptedException e) {
                }
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        Bot bot = new Bot(new File("config.json"));
        bot.connect();
    }
}
