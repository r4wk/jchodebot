package at.mrschlauch.chodebot;

import lombok.Getter;

@Getter
public class DelayedFuncContainer {
    private IDelayedFunc func;
    private long dueTime;
    private long delay;

    public DelayedFuncContainer(IDelayedFunc func, long delay) {
        this.func = func;
        this.delay = delay;
        this.dueTime = System.currentTimeMillis() + delay;
    }

    public boolean isDue() {
        return System.currentTimeMillis() > this.dueTime;
    }

    public DelayedFuncContainer copy() {
        return new DelayedFuncContainer(this.func, this.delay);
    }
}
