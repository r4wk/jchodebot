package at.mrschlauch.chodebot.toposort;

public class CyclicDependencyError extends Exception {
	private static final long serialVersionUID = 686903157750930571L;

	public CyclicDependencyError() {
		super();
	}

	public CyclicDependencyError(String paramString, Throwable paramThrowable,
			boolean paramBoolean1, boolean paramBoolean2) {
		super(paramString, paramThrowable, paramBoolean1, paramBoolean2);
	}

	public CyclicDependencyError(String paramString, Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}

	public CyclicDependencyError(String paramString) {
		super(paramString);
	}

	public CyclicDependencyError(Throwable paramThrowable) {
		super(paramThrowable);
	}
}
