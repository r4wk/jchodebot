package at.mrschlauch.chodebot.toposort;

import java.util.Collection;

public interface IDependant<T extends IDependant<T>> {
	public Collection<T> getDependencies();
}
