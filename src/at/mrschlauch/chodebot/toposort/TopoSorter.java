package at.mrschlauch.chodebot.toposort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;

public class TopoSorter<T extends IDependant<T>> {
	public TopoSorter() {}
	
	public List<T> findRootNodes(Collection<T> nodes) {
		Map<T,Integer> indegrees = new HashMap<T,Integer>();
		
		for (T node: nodes) {
			indegrees.put(node, 0);
		}
		
		for (T node: nodes) {
			for (T dependency: node.getDependencies()) {
				indegrees.put(dependency, indegrees.get(dependency)+1);
			}
		}
		
		List<T> isolates = new ArrayList<T>();
		
		for (T key: indegrees.keySet()) {
			if (indegrees.get(key) == 0) {
				isolates.add(key);
			}
		}
		
		return isolates;
	}
	
	public List<T> sort(Collection<T> nodes) throws CyclicDependencyError {
		List<T> result = new ArrayList<T>();
		
		if (nodes.size() == 0) {
			return result;
		}
		
		List<T> isolates = this.findRootNodes(nodes);
		
		if (isolates.size() == 0) {
			throw new CyclicDependencyError("Involving all nodes");
		}
		
		Set<T> visited = new HashSet<T>();
		
		for (T node: isolates) {
			Set<T> seen = new HashSet<T>();
			visit(node, nodes, result, visited, seen);
		}
		
		return result;
		
	}
	
	private void visit(T node, Collection<T> nodes, List<T> result, Set<T> visited, Set<T> seen) throws CyclicDependencyError {
		if (seen.contains(node)) {
			throw new CyclicDependencyError();
		}
		
		if (visited.contains(node)) {
			return;
		}
		
		seen.add(node);
		
		for (T dependency: node.getDependencies()) {
			visit(dependency, nodes, result, visited, seen);
		}
		
		visited.add(node);
		seen.remove(node);
		
		result.add(node);
	}
	
	public static class Test {
		@Getter
		public static class MockPlugin {
			private String name;
			private String[] dependencies;
			
			public MockPlugin(String name, String... dependencies) {
				this.name = name;
				this.dependencies = dependencies;
			}
		}
		
		@Getter
		public static class MockPluginContainer implements IDependant<MockPluginContainer> {
			private MockPlugin plugin;
			private Set<MockPluginContainer> dependencies = new HashSet<MockPluginContainer>();
			private Set<MockPluginContainer> dependendants = new HashSet<MockPluginContainer>();
			
			public MockPluginContainer(MockPlugin plugin) {
				this.plugin = plugin;
			}
			
			@Override
			public Collection<MockPluginContainer> getDependencies() {
				return dependencies;
			}
		}
		
		public static class MissingDependencyError extends Exception {
			private static final long serialVersionUID = 2824828978187520942L;

			public MissingDependencyError(String paramString) {
				super(paramString);
			}
		}
		
		public static List<MockPluginContainer> test(Collection<MockPlugin> plugins) throws MissingDependencyError, CyclicDependencyError {
			Map<String,MockPluginContainer> pcs = new HashMap<String,MockPluginContainer>();
			
			for (MockPlugin plugin: plugins) {
				pcs.put(plugin.getName(), new MockPluginContainer(plugin));
			}
			
			for (MockPluginContainer pc: pcs.values()) {
				for (String depName: pc.getPlugin().getDependencies()) {
					MockPluginContainer dependency = pcs.get(depName);
					
					if (dependency == null) {
						throw new MissingDependencyError(String.format("Plugin %s depends on missing plugins %s.",
								pc.getPlugin().getName(), depName));
					}
					
					pc.getDependencies().add(dependency);
					dependency.getDependendants().add(pc);
				}
			}
			
			TopoSorter<MockPluginContainer> sorter = new TopoSorter<MockPluginContainer>();
			List<MockPluginContainer> sorted = sorter.sort(pcs.values());
			return sorted;
		}
		
		public static void main(String[] args) {
			try {
				List<MockPluginContainer> pcs = test(Arrays.asList(
						new MockPlugin("db"),
						new MockPlugin("factoids", "tells"),
						new MockPlugin("tells", "factoids"),
						new MockPlugin("factoidtells", "factoids", "tells")));
				for (MockPluginContainer pc: pcs) {
					System.out.print(pc.getPlugin().getName());
					System.out.print(" ");
				}
				System.out.println();
			} catch (MissingDependencyError e) {
				System.out.println("Missing dependency "+e);
			} catch (CyclicDependencyError e) {
				System.out.println("Cyclic Dependency "+e);
			}
		}
	}
}
