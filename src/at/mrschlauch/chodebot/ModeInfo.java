package at.mrschlauch.chodebot;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModeInfo {
    private ModeType type;
    private boolean adding;
    private char mode;
    private String arg;
    private User user;

    public ModeInfo(ModeType type, boolean adding, char mode) {
        this.type = type;
        this.adding = adding;
        this.mode = mode;
    }

    public ModeInfo(ModeType type, boolean adding, char mode, String arg) {
        this(type, adding, mode);
        this.arg = arg;
    }

    public ModeInfo(ModeType type, boolean adding, char mode, User user) {
        this(type, adding, mode);
        this.user = user;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (this.adding) {
            sb.append("+");
        } else {
            sb.append("-");
        }

        sb.append(this.mode);

        if (this.arg != null || this.user != null) {
            sb.append(" ");
            if (this.arg != null) {
                sb.append(this.arg);
            } else {
                sb.append(this.user.toString());
            }
        }

        return sb.toString();
    }
}
