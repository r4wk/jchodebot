package at.mrschlauch.chodebot;

import java.util.HashSet;
import java.util.Set;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private Connection connection;
    private String nick;
    private String username;
    private String host;
    private String realname;
    private String nickservAccount;
    private Account account;
    private Set<Channel> channels = new HashSet<Channel>();
    private boolean synced;
    private long syncTime;

    public User(Connection conn, String nick) {
        this.connection = conn;
        this.nick = nick;
    }

    public User(Connection conn, String nick, String username, String host) {
        this(conn, nick);
        this.username = username;
        this.host = host;
    }

    @Override
    public String toString() {
        return String.format("<User %s>", this.getMask());
    }

    public void removeChannel(Channel channel) {
        assert this.channels.contains(channel);
        this.channels.remove(channel);
    }

    public void addChannel(Channel channel) {
        assert !this.channels.contains(channel);
        this.channels.add(channel);
    }

    public String getMask() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.nick);
        sb.append('!');
        sb.append(this.username);
        sb.append('@');
        sb.append(this.host);
        return sb.toString();
    }
}
