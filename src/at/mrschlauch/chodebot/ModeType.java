package at.mrschlauch.chodebot;

public enum ModeType {
    LIST, ARG_ALWAYS, ARG_ON_SET, ARG_NEVER, STATUS, UMODE;
}
