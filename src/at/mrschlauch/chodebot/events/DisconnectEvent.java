package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Connection;

@Getter
public class DisconnectEvent extends Event {
    private Connection connection;
    
    public DisconnectEvent(Connection conn) {
        this.connection = conn;
    }
}
