package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.ModeInfo;

@Getter
public class ModeEvent extends IRCRecvEvent {
    private String target;
    private ModeInfo modeInfo;
    
    public ModeEvent(IRCLine line, String target, ModeInfo info) {
        super(line);
        this.target = target;
        this.modeInfo = info;
    }
}
