package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.User;

@Getter
public class NickEvent extends IRCRecvEvent {
    private User user;
    private String oldNick;
    private String newNick;
    
    public NickEvent(IRCLine line, User user, String oldNick, String newNick) {
        super(line);
        this.user = user;
        this.oldNick = oldNick;
        this.newNick = newNick;
    }
}
