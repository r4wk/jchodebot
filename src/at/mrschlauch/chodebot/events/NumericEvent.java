package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.IRCLine;

@Getter
public class NumericEvent extends IRCRecvEvent {
    private int numeric;

    public NumericEvent(IRCLine line, int numeric) {
        super(line);
        this.numeric = numeric;
    }
}
