package at.mrschlauch.chodebot.events;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.mrschlauch.chodebot.Bot;

public class EventRegistry {
    private static EventRegistry instance = new EventRegistry();
	private Map<Class<? extends Event>,List<EventHandler>> handlers = new HashMap<Class<? extends Event>,List<EventHandler>>();
	private Map<Class<? extends Event>,List<Class<? extends Event>>> inheritanceCache = new HashMap<Class<? extends Event>,List<Class<? extends Event>>>();
	
	public static EventRegistry getInstance() {
	    return instance;
	}
	
	public EventRegistry() {
		registerEvents();
	}
	
	private void registerEvents() {
		this.registerEvent(Event.class);
		this.registerEvent(ConnectEvent.class);
		this.registerEvent(DisconnectEvent.class);
		this.registerEvent(IRCRecvEvent.class);
		this.registerEvent(MessageEvent.class);
		this.registerEvent(NumericEvent.class);
		this.registerEvent(UnknownCommandEvent.class);
		this.registerEvent(CTCPEvent.class);
		this.registerEvent(ActionEvent.class);
		this.registerEvent(NoticeEvent.class);
		this.registerEvent(CTCPResponseEvent.class);
		this.registerEvent(ModeEvent.class);
		this.registerEvent(UserModeEvent.class);
		this.registerEvent(ChannelModeEvent.class);
		this.registerEvent(JoinEvent.class);
		this.registerEvent(PartEvent.class);
		this.registerEvent(KickEvent.class);
		this.registerEvent(QuitEvent.class);
		this.registerEvent(NickEvent.class);
	}
	
	public void registerEvent(Class<? extends Event> cls) {
		if (this.handlers.containsKey(cls)) {
			Bot.logger.severe(String.format("Trying to reregister event %s. Not registering event.", cls.getSimpleName()));
			return;
		}
		
		List<Class<? extends Event>> inheritance = this.listInheritance(cls);
		this.inheritanceCache.put(cls, inheritance);
		this.handlers.put(cls, new ArrayList<EventHandler>());
	}
	
	@SuppressWarnings("unchecked")
	private List<Class<? extends Event>> listInheritance(Class<? extends Event> cls) {
		List<Class<? extends Event>> inheritance = new ArrayList<Class<? extends Event>>();
		inheritance.add(cls);
		
		while (cls != Event.class) {
			cls = (Class<? extends Event>) cls.getSuperclass();
			if (this.handlers.containsKey(cls)) {
				inheritance.add(cls);
			}
		}
		
		return inheritance;
	}
	
	@SuppressWarnings("unchecked")
	public void registerHandlers(Object obj) {
		for (Method m: obj.getClass().getDeclaredMethods()) {
			Hook ann = m.getAnnotation(Hook.class);
			
			if (ann == null) {
				continue;
			}
			
			if ((m.getModifiers() & Modifier.PUBLIC) == 0) {
				continue;
			}
			
			String handlerName = obj.getClass().getName()+"."+m.getName();
			
			Class<?>[] paramTypes = m.getParameterTypes();
			
			if (paramTypes.length != 1 || !Event.class.isAssignableFrom(paramTypes[0])) {
				Bot.logger.severe(String.format("Not registering event handler %s: Handler has wrong parameter types: %s",
						handlerName, Arrays.toString(paramTypes)));
				continue;
			}
			
			Class<? extends Event> cls = (Class<? extends Event>)paramTypes[0];
			List<EventHandler> eventHandlers = this.handlers.get(cls);
			
			if (eventHandlers == null) {
				Bot.logger.severe(String.format("Not registering event handler %s for event %s: Event is not registered!", handlerName, cls.getSimpleName()));
				continue;
			}
		
			EventHandler handler = new EventHandler(obj, m);
			eventHandlers.add(handler);
			Collections.sort(eventHandlers);
			Bot.logger.info(String.format("Registered event handler %s for event %s.", handlerName, cls.getSimpleName()));
		}
	}
	
	public void fire(Event event) {
		Class<? extends Event> cls = event.getClass();
		List<Class<? extends Event>> inheritance = this.inheritanceCache.get(cls);
		
		if (inheritance == null) {
			Bot.logger.severe(String.format("Not firing event %s because it's not registered!", cls.getSimpleName()));
			return;
		}
		
		for (Class<? extends Event> clazz: inheritance) {
			for (EventHandler handler: this.handlers.get(clazz)) {
				try {
					handler.call(event);
				} catch (Exception e) {
					Bot.logger.severe(String.format("Error handling event %s.", clazz.getSimpleName()));
					Bot.logger.throwing("EventRegistry", "fireEvent", e);
				}
			}
		}
	}
}
