package at.mrschlauch.chodebot.events;

import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.TargetType;
import at.mrschlauch.chodebot.User;

public class ActionEvent extends CTCPEvent {
    public ActionEvent(IRCLine line, String target, TargetType targetType, String command, String message, User user, Channel channel) {
        super(line, target, targetType, command, message, user, channel);
    }
}
