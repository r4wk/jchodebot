package at.mrschlauch.chodebot.events;

import at.mrschlauch.chodebot.IRCLine;

public class UnknownCommandEvent extends IRCRecvEvent {
    public UnknownCommandEvent(IRCLine line) {
        super(line);
    }
}
