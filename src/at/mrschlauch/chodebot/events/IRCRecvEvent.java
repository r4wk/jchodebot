package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.IRCLine;

@Getter
public class IRCRecvEvent extends Event {
	private IRCLine line;
	
	public IRCRecvEvent(IRCLine line) {
		this.line = line;
	}

}
