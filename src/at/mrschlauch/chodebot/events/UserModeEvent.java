package at.mrschlauch.chodebot.events;

import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.ModeInfo;

public class UserModeEvent extends ModeEvent {
    public UserModeEvent(IRCLine line, String target, ModeInfo info) {
        super(line, target, info);
    }
}
