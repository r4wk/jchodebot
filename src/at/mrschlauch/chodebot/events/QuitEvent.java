package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.User;

@Getter
public class QuitEvent extends IRCRecvEvent {
    private User user;
    private String reason;

    public QuitEvent(IRCLine line, User user, String reason) {
        super(line);
        this.user = user;
        this.reason = reason;
    }

}
