package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.TargetType;
import at.mrschlauch.chodebot.User;

@Getter
public class CTCPEvent extends IRCRecvEvent {
    private String target;
    private TargetType targetType;
    private String command;
    private String message;
    private Channel channel;
    private User user;

    public CTCPEvent(IRCLine line, String target, TargetType targetType, String command, String message, User user, Channel channel) {
        super(line);
        this.target = target;
        this.targetType = targetType;
        this.command = command;
        this.message = message;
        this.user = user;
        this.channel = channel;
    }
}
