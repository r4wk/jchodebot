package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.ModeInfo;

@Getter
public class ChannelModeEvent extends ModeEvent {
    private Object setter;
    private Channel channel;

    public ChannelModeEvent(IRCLine line, String target, ModeInfo info, Channel channel, Object setter) {
        super(line, target, info);
        this.channel = channel;
        this.setter = setter;
    }
}