package at.mrschlauch.chodebot.events;

import lombok.Getter;

public class Event {
	@Getter private boolean cancelled;
	
	public void cancel() {
		this.cancelled = true;
	}
}
