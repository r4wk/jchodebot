package at.mrschlauch.chodebot.events;

import java.lang.reflect.Method;

public class EventHandler implements Comparable<EventHandler> {
	private Object obj;
	private Method method;
	protected Priority priority = Priority.NORMAL;
	
	public EventHandler(Object obj, Method m) {
		this.obj = obj;
		this.method = m;
	}
	
	@Override
	public int compareTo(EventHandler that) {
		return this.priority.ordinal() < that.priority.ordinal() ? -1 :
			   this.priority.ordinal() > that.priority.ordinal() ? 1 : 0;
	}
	
	public void call(Event event) throws Exception {
		this.method.invoke(obj, event);
	}
}
