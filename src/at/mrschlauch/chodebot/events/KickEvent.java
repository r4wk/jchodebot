package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.User;

@Getter
public class KickEvent extends IRCRecvEvent {
    private Channel channel;
    private User recipient;
    private Object kicker;

    public KickEvent(IRCLine line, Channel channel, User recipient, Object kicker) {
        super(line);
        this.channel = channel;
        this.recipient = recipient;
        this.kicker = kicker;
    }

}
