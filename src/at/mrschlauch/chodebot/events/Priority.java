package at.mrschlauch.chodebot.events;

public enum Priority {
	HIGHEST,
	HIGHER,
	NORMAL,
	LOWER,
	LOWEST;
}
