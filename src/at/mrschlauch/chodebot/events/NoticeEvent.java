package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.TargetType;
import at.mrschlauch.chodebot.User;

@Getter
public class NoticeEvent extends IRCRecvEvent {
    private String target;
    private TargetType targetType;
	private String message;
	private User user;
	private Channel channel;

	public NoticeEvent(IRCLine line, String target, TargetType targetType, String message, User user, Channel channel) {
		super(line);
		this.target = target;
		this.targetType = targetType;
		this.message = message;
		this.user = user;
		this.channel = channel;
	}
}
