package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Connection;

@Getter
public class ConnectEvent extends Event {
    private Connection connection;
    
    public ConnectEvent(Connection connection) {
        this.connection = connection;
    }
}
