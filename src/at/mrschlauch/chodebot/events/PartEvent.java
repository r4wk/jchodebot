package at.mrschlauch.chodebot.events;

import lombok.Getter;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.IRCLine;
import at.mrschlauch.chodebot.User;

@Getter
public class PartEvent extends IRCRecvEvent {
    private Channel channel;
    private User user;
    
    public PartEvent(IRCLine line, Channel channel, User user) {
        super(line);
        this.channel = channel;
        this.user = user;
    }

}
