package at.mrschlauch.chodebot.plugins.core;

import java.util.ArrayList;
import java.util.List;

import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.Connection;
import at.mrschlauch.chodebot.User;
import at.mrschlauch.chodebot.Utils;
import at.mrschlauch.chodebot.events.EventRegistry;
import at.mrschlauch.chodebot.plugins.Init;
import at.mrschlauch.chodebot.plugins.Plugin;
import at.mrschlauch.chodebot.plugins.commands.Command;
import at.mrschlauch.chodebot.plugins.commands.CommandInfo;
import at.mrschlauch.chodebot.plugins.commands.Commands;

@Plugin(name = "Core", description = "Provides commonly used bot commands.", dependencies = {"Commands"})
public class Core {
    @Init
    public void init() {
        System.out.println("Loaded plugin Core \\o/");
        EventRegistry.getInstance().registerHandlers(this);
        Commands commands = (Commands)Bot.getInstance().getPluginLoader().getPlugin("Commands");
        commands.registerCommands(this);
    }

    @Command(name = "say", help = "say <message> - replies with the given message")
    public void handleSay(CommandInfo info) {
        info.reply(info.getInput());
    }
    
    @Command(name = "me", help = "me <message> - sends an action with the given message")
    public void handleMe(CommandInfo info) {
        info.action(info.getInput());
    }
    
    @Command(name = "dump", help = "dump <raw> - sends raw line to the IRC server", adminOnly = true)
    public void handleDump(CommandInfo info) {
        Bot.getInstance().getConnection().sendQueued(info.getInput());
    }
    
    @Command(name = "mystatus", help = "mystatus - replies with your current channel statuses")
    public void handleMyStatus(CommandInfo info) {
        Channel channel = info.getChannel();
        
        if (channel == null) {
            info.notice("This commands only works in a channel.");
            return;
        }
        
        User user = info.getUser();
        List<String> statuses = new ArrayList<String>();
        
        if (channel.getOwners().contains(user)) {
            statuses.add("owner");
        }
        
        if (channel.getSuperOps().contains(user)) {
            statuses.add("super op");
        }
        
        if (channel.getOps().contains(user)) {
            statuses.add("op");
        }
        
        if (channel.getHalfOps().contains(user)) {
            statuses.add("half op");
        }
        
        if (channel.getVoiced().contains(user)) {
            statuses.add("voiced");
        }
        
        String reply;
        
        if (statuses.size() == 0) {
            reply = String.format("You have no status in %s.", channel.getName());
        } else {
            reply = String.format("You are %s in %s.", Utils.join(", ", statuses), channel.getName());
        }
        
        info.reply(reply);
    }

    @Command(name = "part", help = "part <#channel> - parts the given channel", adminOnly = true)
    public void handlePart(CommandInfo info) {
        String channelName = info.getInput();
        
        Connection conn = Bot.getInstance().getConnection();

        if (conn.getUserTracker().getChannel(channelName) == null) {
            info.notice("I am not on that channel.");
        }

        info.notice(String.format("Parting %s.", channelName));
        conn.sendQueued(String.format("PART %s", channelName));
    }
    
    @Command(name = "die", help = "die [<reason>]- quits from the IRC server", adminOnly = true)
    public void handleDie(CommandInfo info) {
        String reason = info.getInput();
        
        if (reason.isEmpty()) {
            reason = "requested";
        }
        
        info.notice("Bye o/~");
        Connection conn = Bot.getInstance().getConnection();
        conn.setReconnecting(false);
        conn.quit(reason);
    }
    
    @Command(name = "reconnect", help = "reconnect [<reason>]- reconnects to the IRC server", adminOnly = true)
    public void handleReconnect(CommandInfo info) {
        String reason = info.getInput();
        
        if (reason.isEmpty()) {
            reason = "BRB";
        }
        
        info.notice("Bye o/~");
        Connection conn = Bot.getInstance().getConnection();
        conn.quit(reason);
    }

    @Command(name = "join", help = "join <#channel> - joins the given channel", adminOnly = true)
    public void handleJoin(CommandInfo info) {
        String channelName = info.getInput();

        if (Bot.getInstance().getConnection().getUserTracker()
                .getChannel(channelName) != null) {
            info.notice("I am already on that channel.");
        }

        info.notice(String.format("Joining %s.", channelName));
        Bot.getInstance().getConnection()
                .sendQueued(String.format("JOIN %s", channelName));
    }
}
