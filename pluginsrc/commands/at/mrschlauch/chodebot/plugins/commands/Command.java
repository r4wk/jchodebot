package at.mrschlauch.chodebot.plugins.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})	
public @interface Command {
	public String name();
	public String help() default "No help available.";
	public boolean adminOnly() default false;
}