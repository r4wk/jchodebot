package at.mrschlauch.chodebot.plugins.commands;

import lombok.Getter;
import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.User;

@Getter
public class CommandInfo {
	private String name;
	private String input;
	private User user;
	private Channel channel;
	private CommandHandler handler;
	private long requestTime;
	
	public CommandInfo(String name, String input, User user, Channel channel, CommandHandler handler) {
		this.name = name;
		this.input = input;
		this.user = user;
		this.channel = channel;
		this.handler = handler;
		this.requestTime = System.currentTimeMillis();
	}
	
	public void showHelp() {
	    Commands commands = (Commands) Bot.getInstance().getPluginLoader().getPlugin("Commands");
	    CommandHandler handler = commands.getHandler(this.name);
	    
	    if (handler == null) {
	        return;
	    }
	    
	    for (String line: handler.getHelpText().split("\n")) {
	        line = line.trim();
	        if (line.isEmpty()) {
	            continue;
	        }
	        this.notice(line);
	    }
	}
	
	public void reply(String message) {
		String target;
		
		if (this.channel != null) {
			target = this.channel.getName();
		} else {
			target = this.user.getNick();
		}
		
		Bot.getInstance().getConnection().sendMessage(target, message);
	}
	
	public void respond(String message) {
		this.reply(String.format("%s: %s", this.user.getNick(), message));
	}
	
	public void notice(String message) {
		Bot.getInstance().getConnection().sendNotice(this.user.getNick(), message);
	}
	
	public void action(String message) {
		String target;
		
		if (this.channel != null) {
			target = this.channel.getName();
		} else {
			target = this.user.getNick();
		}
		
		Bot.getInstance().getConnection().sendAction(target, message);
	}
}