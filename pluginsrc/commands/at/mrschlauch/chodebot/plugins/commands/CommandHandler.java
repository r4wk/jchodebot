package at.mrschlauch.chodebot.plugins.commands;

import java.lang.reflect.Method;

import lombok.Getter;

@Getter
public class CommandHandler {
	private String name;
	private Object instance;
	private Method method;
	private String helpText;
	private boolean isAdminOnly;
	
	public CommandHandler(String name, Object instance, Method method, String helpText, boolean isAdminOnly) {
		this.name = name;
		this.instance = instance;
		this.method = method;
		this.helpText = helpText;
		this.isAdminOnly = isAdminOnly;
	}
	
	public void call(CommandInfo info) throws Exception {
		this.method.invoke(this.instance, info);
	}
}