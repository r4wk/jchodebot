package at.mrschlauch.chodebot.plugins.commands;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;

@Getter
public class Config {
    private Set<String> commandTriggers = new HashSet<String>();
    
    public Config() {
        commandTriggers.add("/");
    }
}
