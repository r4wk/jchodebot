package at.mrschlauch.chodebot.plugins.commands;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;

import at.mrschlauch.chodebot.Account;
import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.User;
import at.mrschlauch.chodebot.events.EventRegistry;
import at.mrschlauch.chodebot.events.Hook;
import at.mrschlauch.chodebot.events.MessageEvent;
import at.mrschlauch.chodebot.events.NumericEvent;
import at.mrschlauch.chodebot.plugins.Init;
import at.mrschlauch.chodebot.plugins.Plugin;
import at.mrschlauch.chodebot.plugins.PluginUtils;


@Plugin(name = "Commands", description = "Provides command handling.")
public class Commands {
	private static Class<?>[] handlerParamTypes = new Class<?>[] {CommandInfo.class};
	private Map<String,CommandHandler> handlers = new HashMap<String,CommandHandler>();
    private LinkedList<CommandInfo> pendingCommands = new LinkedList<CommandInfo>();
    private Config config;
	
	public Commands() {
	}
	
	@Init
	public void init() {
	    EventRegistry.getInstance().registerHandlers(this);
	    this.config = PluginUtils.loadOrCreateConfig("Commands", Config.class, new Config());
	    this.registerCommands(this);
	}
	
	@Hook
	public void onNumeric(NumericEvent e) {
	    if (e.getNumeric() != 315) {
	        return;
	    }
	    
        long now = System.currentTimeMillis();

        for (ListIterator<CommandInfo> it = this.pendingCommands.listIterator(); it.hasNext();) {
            CommandInfo info = it.next();
            User user = info.getUser();

            if (user.isSynced()) {
                Account account = user.getAccount();
                if (account != null && account.isAdmin()) {
                    this.callCommand(info);
                } else {
                    info.notice("You don't have permission.");
                }

                it.remove();
            } else if (now - info.getRequestTime() > 30 * 1000) {
                it.remove();
            }
        }
	}
	
	public void registerCommands(Object obj) {
		for (Method m: obj.getClass().getDeclaredMethods()) {
			Command ann = m.getAnnotation(Command.class);
			
			if (ann == null) {
				continue;
			}
			
			if ((m.getModifiers() & Modifier.PUBLIC) == 0) {
				continue;
			}
			
			Class<?>[] paramTypes = m.getParameterTypes();
			
			if (!Arrays.equals(paramTypes, handlerParamTypes)) {
				Bot.logger.warning(String.format("Trying to register command %s with wrong method parameters: %s (%s expected)",
						ann.name(), Arrays.toString(paramTypes), Arrays.toString(handlerParamTypes)));
				continue;
			}
			
			if (this.handlers.containsKey(ann.name())) {
				Bot.logger.warning("Trying to register already registered command "+ann.name());
				continue;
			}
			
			CommandHandler handler = new CommandHandler(ann.name(), obj, m, ann.help(), ann.adminOnly());
			this.handlers.put(ann.name(), handler);
			Bot.logger.info(String.format("Registered command %s.", ann.name()));
		}
	}
	
	@Hook
	public void onMessage(MessageEvent e) {
	    String message = e.getMessage();
	    for (String prefix: this.config.getCommandTriggers()) {
    		if (!message.startsWith(prefix) || message.equals(prefix)) {
    			continue;
    		}
    		
    		int space = message.indexOf(" ", prefix.length());
    			
    		String cmd;
    		String inp;
    		
    		if (space == -1) {
    			cmd = message.substring(prefix.length());
    			inp = "";
    		} else {
    			cmd = message.substring(prefix.length(), space);
    			inp = message.substring(space+1);
    		}
    		
    		this.dispatch(cmd, inp, e.getUser(), e.getChannel());
    		break;
	    }
	}
	
	public CommandHandler getHandler(String commandName) {
	    return this.handlers.get(commandName);
	}
	
	private void dispatch(String name, String input, User user, Channel channel) {
		CommandHandler handler = this.handlers.get(name);
		
		if (handler == null) {
			Bot.logger.info(String.format("%s tried to invoke unknown command %s", user, name));
			return;
		}
		
		CommandInfo info = new CommandInfo(name, input, user, channel, handler);
		
		if (handler.isAdminOnly()) {
			if (user.isSynced()) {
				Account account = user.getAccount();
				if (account != null && account.isAdmin()) {
					this.callCommand(info);
				} else {
					info.notice("You don't have permission.");
				}
			} else {
			    String cmd = String.format("WHO %s %%uhnar", user.getNick());
			    Bot.getInstance().getConnection().sendQueued(cmd);
				this.scheduleCommand(info);
			}
			return;
		}
		
		callCommand(info);
	}
	
	private void callCommand(CommandInfo info) {
		try {
			info.getHandler().call(info);
		} catch (Exception e) {
			Bot.logger.warning(String.format("Failed to invoke command %s", e));
			Bot.logger.throwing("Commands", "call", e);
		}
	}
	
	private void scheduleCommand(CommandInfo info) {
	    this.pendingCommands.add(info);
	}
	
    @Command(name = "help", help = "help - shows this help\nhelp <command> - shows help for command")
    public void handleHelp(CommandInfo info) {
        String commandName = info.getInput().trim();
        
        if (commandName.isEmpty()) {
            info.showHelp();
            return;
        }
        
	    CommandHandler handler = this.getHandler(commandName);
	    
	    if (handler == null) {
	        info.notice("No help available for command "+commandName);
	        return;
	    }
	    
	    for (String line: handler.getHelpText().split("\n")) {
	        line = line.trim();
	        if (line.isEmpty()) {
	            continue;
	        }
	        info.notice(line);
	    }
    }
}
