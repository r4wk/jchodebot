package at.mrschlauch.chodebot.plugins.derptriggers;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import at.mrschlauch.chodebot.Bot;
import static at.mrschlauch.chodebot.Utils.equalsIgnoreIRCCase;
import at.mrschlauch.chodebot.plugins.Init;
import at.mrschlauch.chodebot.plugins.Plugin;
import at.mrschlauch.chodebot.plugins.abstracts.Abstracts;
import at.mrschlauch.chodebot.plugins.triggers.ActionTrigger;
import at.mrschlauch.chodebot.plugins.triggers.Trigger;
import at.mrschlauch.chodebot.plugins.triggers.TriggerInfo;
import at.mrschlauch.chodebot.plugins.triggers.Triggers;

@Plugin(name = "DerpTriggers", description = "Provides silly triggers.", dependencies = {"Triggers", "Abstracts"})
public class DerpTriggers {
    @Init
    public void init() {
        Triggers triggers = (Triggers)Bot.getInstance().getPluginLoader().getPlugin("Triggers");
        triggers.registerTriggers(this);
    }
    
    @ActionTrigger("(?:hugs|cuddles|snuggles|glomps|squeezes|huggles) +([^ ]+) *")
    public void onHug(TriggerInfo info) {
        String whom = info.getMatcher().group(1);
        String currentNick = Bot.getInstance().getConnection().getCurrentNick();
        
        if (!equalsIgnoreIRCCase(whom, currentNick)) {
            return;
        }
        
        Map<String,String> variables = new HashMap<String,String>();
        String user = info.getUser().getNick();
        variables.put("user", user);
        Abstracts abstracts = (Abstracts)Bot.getInstance().getPluginLoader().getPlugin("Abstracts");
        String interpolated = abstracts.interpolate("$hugReactions", variables);
        
        if (interpolated.startsWith("/")) {
            info.action(interpolated.substring(1));
        } else {
            info.reply(interpolated);
        }
    }
    
    @ActionTrigger("(?:attacks|hits|kicks|slaps|punches) +([^ ]+).*")
    public void onAttack(TriggerInfo info) {
        String whom = info.getMatcher().group(1);
        String currentNick = Bot.getInstance().getConnection().getCurrentNick();
        
        if (!equalsIgnoreIRCCase(whom, currentNick)) {
            return;
        }
        
        Map<String,String> variables = new HashMap<String,String>();
        String user = info.getUser().getNick();
        variables.put("user", user);
        Abstracts abstracts = (Abstracts)Bot.getInstance().getPluginLoader().getPlugin("Abstracts");
        String interpolated = abstracts.interpolate("$attackReactions", variables);
        
        if (interpolated.startsWith("/")) {
            info.action(interpolated.substring(1));
        } else {
            info.reply(interpolated);
        }
    }
    
    @Trigger("(?i)r")
    public void onR(TriggerInfo info) {
        String reply = String.format("r %s %s", info.getUser().getNick(), Triggers.makeSmily());
        info.reply(reply);
    }
    
    @Trigger("(?i)derp")
    public void onDerp(TriggerInfo info) {
        info.reply("ヽ( ｡ヮﾟ)ノ");
    }
    
    @ActionTrigger("(?i)(herps|derps) +at +([^ ]+) *")
    public void onDerpAt(TriggerInfo info) {
        Matcher m = info.getMatcher();
        String herpsderps = m.group(1);
        String whom = m.group(2);
        String currentNick = Bot.getInstance().getConnection().getCurrentNick();
        
        if (!equalsIgnoreIRCCase(whom, currentNick)) {
            return;
        }
        
        String derpsherps;
        if (herpsderps.equals("herps")) {
            derpsherps = "derps";
        } else {
            derpsherps = "herps";
        }
        
        String reply = String.format("%s at %s", derpsherps, info.getUser().getNick());
        info.action(reply);
    }
}
