package at.mrschlauch.chodebot.plugins.abstracts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.plugins.Plugin;

@Plugin(name = "Abstracts", description = "Provides recursively interpolated variables.")
public class Abstracts {
    private static final Pattern interpolatePattern = Pattern.compile("\\$\\{([^}]+)\\}|\\$(\\w+)");
    private static final File abstractsDir = new File("abstracts");
    private final Map<String,List<String>> abstracts = new HashMap<String,List<String>>();
    private final Map<String,Long> abstractsMtimes = new HashMap<String,Long>();
    private final Random rand = new Random();

    public Abstracts() {
    }
    
    public void loadAbstracts() {
        for (File f: abstractsDir.listFiles()) {
            if (!f.getName().endsWith(".txt") || f.isDirectory())

                continue;
            String name = f.getName().substring(f.getName().lastIndexOf("."));
            List<String> choices = loadAbstract(f);
            if (choices.size() == 0)
                continue;
            abstracts.put(name, choices);
            abstractsMtimes.put(name, f.lastModified());
        }
    }
    
    public List<String> loadAbstract(File f) {
        List<String> choices = new ArrayList<String>();

        BufferedReader in = null;
        
        try {

            in = new BufferedReader(new FileReader(f));
            String line = null;
            while ((line = in.readLine()) != null) {
                choices.add(line);

            }
        } catch (IOException e) {
            Bot.logger.severe("Failed to load abstracts from "+f);
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException e) {
            }
        }
        
        return choices;
    }

    public List<String> getChoices(String name) {
        File f = new File(abstractsDir, name+".txt");
        Long mtime = abstractsMtimes.get(name);

        if (f.exists() && (mtime == null || f.lastModified() > mtime)) {
            List<String> choices = loadAbstract(f);
            if (choices.size() == 0) {
                abstracts.remove(name);
                return null;
            }
            abstracts.put(name, choices);
            abstractsMtimes.put(name, f.lastModified());
            return choices;
        }
        
        return abstracts.get(name);
    }

    public String getChoice(String name) {
        List<String> choices = getChoices(name);
        if (choices == null)
            return null;

        return choices.get(rand.nextInt(choices.size()));
    }
    
    public String interpolate(String format) {
        Map<String,String> variables = Collections.emptyMap();
        return this.interpolate(format, variables);
    }
    
    public String interpolate(String format, Map<String,String> variables) {
        StringBuilder sb = new StringBuilder();
        Matcher m = interpolatePattern.matcher(format);
        int previousEnd = 0;
        
        while (m.find()) {
            sb.append(format.substring(previousEnd, m.start()));
            String key = m.group(1) != null ? m.group(1) : m.group(2);
            String val = variables.get(key);
            
            if (val == null) {
                val = this.getChoice(key);
                if (val == null) {
                    val = m.group(0);
                } else {
                    val = this.interpolate(val, variables);
                }
            }
            
            sb.append(val);
            previousEnd = m.end();
        }
        sb.append(format.substring(previousEnd, format.length()));
        
        return sb.toString();
    }
}
