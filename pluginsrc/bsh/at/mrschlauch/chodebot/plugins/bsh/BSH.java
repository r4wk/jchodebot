package at.mrschlauch.chodebot.plugins.bsh;

import bsh.Interpreter;
import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.plugins.Init;
import at.mrschlauch.chodebot.plugins.Plugin;
import at.mrschlauch.chodebot.plugins.commands.Command;
import at.mrschlauch.chodebot.plugins.commands.CommandInfo;
import at.mrschlauch.chodebot.plugins.commands.Commands;

@Plugin(name = "BSH", description = "Provides a BeanShell interpreter command.", dependencies = {"Commands"})
public class BSH {
    @Init
    public void init() {
        Commands commands = (Commands)Bot.getInstance().getPluginLoader().getPlugin("Commands");
        commands.registerCommands(this);
    }
    
    @Command(name = "bsh", help = "bsh <code> - evaluates code on the bot", adminOnly = true)
    public void handleBSH(CommandInfo info) {
        String code = info.getInput();
        
        if (code.isEmpty()) {
            info.notice("No code provided.");
            return;
        }
        
        Interpreter interp = new Interpreter();
        
        try {
            interp.set("bot", Bot.getInstance());
            interp.set("conn", Bot.getInstance().getConnection());
            interp.set("cmd", info);
            Object obj = interp.eval(code);
            
            if (obj != null) {
                info.reply(format(obj));
            }
        } catch (Exception e) {
            Bot.logger.warning("Failed to evaluate BSH code "+code);
            Bot.logger.throwing("BSH", "handleBSH", e);
            info.notice("ERROR: "+e.toString());
        }
    }
    
    private String format(Object obj) {
        return obj.toString();
    }
}
