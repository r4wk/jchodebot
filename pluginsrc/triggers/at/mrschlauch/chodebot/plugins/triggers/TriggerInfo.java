package at.mrschlauch.chodebot.plugins.triggers;

import java.util.regex.Matcher;

import lombok.Getter;
import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.User;

@Getter
public class TriggerInfo {
    private User user;
    private Channel channel;
    private Matcher matcher;
    private TriggerHandler handler;
    
    public TriggerInfo(User user, Channel channel, Matcher matcher, TriggerHandler handler) {
        this.user = user;
        this.channel = channel;
        this.matcher = matcher;
        this.handler = handler;
    }
    
	public void reply(String message) {
		String target;
		
		if (this.channel != null) {
			target = this.channel.getName();
		} else {
			target = this.user.getNick();
		}
		
		Bot.getInstance().getConnection().sendMessage(target, message);
	}
	
	public void respond(String message) {
		this.reply(String.format("%s: %s", this.user.getNick(), message));
	}
	
	public void notice(String message) {
		Bot.getInstance().getConnection().sendNotice(this.user.getNick(), message);
	}
	
	public void action(String message) {
		String target;
		
		if (this.channel != null) {
			target = this.channel.getName();
		} else {
			target = this.user.getNick();
		}
		
		Bot.getInstance().getConnection().sendAction(target, message);
	}
}
