package at.mrschlauch.chodebot.plugins.triggers;

import java.lang.reflect.Method;
import java.util.regex.Pattern;

import lombok.Getter;

public class TriggerHandler {
    private Object obj;
    private Method method;
    @Getter private Pattern pattern;
    @Getter private String name;
    @Getter private boolean action;
    
    public TriggerHandler(Object obj, Method method, Pattern pattern, boolean action) {
        this.obj = obj;
        this.method = method;
        this.pattern = pattern;
        this.action = action;
        this.name = obj.getClass().getName()+"."+method.getName();
    }
    
    public void call(TriggerInfo info) throws Exception {
        this.method.invoke(this.obj, info);
    }
}
