package at.mrschlauch.chodebot.plugins.triggers;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.Channel;
import at.mrschlauch.chodebot.User;
import at.mrschlauch.chodebot.events.ActionEvent;
import at.mrschlauch.chodebot.events.EventRegistry;
import at.mrschlauch.chodebot.events.Hook;
import at.mrschlauch.chodebot.events.MessageEvent;
import at.mrschlauch.chodebot.plugins.Init;
import at.mrschlauch.chodebot.plugins.Plugin;

@Plugin(name = "Triggers", description = "Provides canned responses triggered by regular expressions.")
public class Triggers {
    private static Class<?>[] handlerParamTypes = new Class<?>[] {TriggerInfo.class};
    private static String[] eyes = { ":", ";", "=", "8" };
    private static String[] noses = { "-", "" };
    private static String[] mouths = { ">", ")", "D", "]" };
    public static String nickPattern = "[a-zA-Z_\\[\\]\\\\{}|`^][a-zA-Z_0-9\\[\\]\\\\{}|`^-]*";
    private static Random rand = new Random();
    private Set<TriggerHandler> handlers = new HashSet<TriggerHandler>();

    public static String makeSmily() {
        return eyes[rand.nextInt(eyes.length)]
               + noses[rand.nextInt(noses.length)]
               + mouths[rand.nextInt(mouths.length)];
    }
    
    @Init
    public void init() {
        EventRegistry.getInstance().registerHandlers(this);
    }

	public void registerTriggers(Object obj) {
		for (Method m: obj.getClass().getDeclaredMethods()) {
			String handlerName = obj.getClass().getName()+"."+m.getName();
			
			Trigger trigger = m.getAnnotation(Trigger.class);
			ActionTrigger actionTrigger = m.getAnnotation(ActionTrigger.class);
			
			if (trigger != null && actionTrigger != null) {
			    Bot.logger.severe(String.format("Not registering trigger %s: can't have both @Trigger and @ActionTrigger annotations", handlerName));
			    continue;
			}
			
			if (trigger == null && actionTrigger == null) {
			    continue;
			}
			
			boolean action;
			String regex;
			
			if (trigger != null) {
			    regex = trigger.value();
			    action = false;
			} else {
			    regex = actionTrigger.value();
			    action = true;
			}
			
			if ((m.getModifiers() & Modifier.PUBLIC) == 0) {
				continue;
			}
			
			Class<?>[] paramTypes = m.getParameterTypes();
			
			if (!Arrays.equals(paramTypes, handlerParamTypes)) {
				Bot.logger.warning(String.format("Trying to register trigger %s with wrong method parameters: %s (%s expected)",
						handlerName, Arrays.toString(paramTypes), Arrays.toString(handlerParamTypes)));
				continue;
			}
			
			Pattern pattern;
			try {
			    pattern = Pattern.compile(regex);
			} catch (PatternSyntaxException e) {
			    Bot.logger.warning(String.format("Not registering trigger %s: bad pattern (%s).", handlerName, e));
			    continue;
			}
			
			TriggerHandler handler = new TriggerHandler(obj, m, pattern, action);
			this.handlers.add(handler);
			Bot.logger.info(String.format("Registered trigger %s.", handlerName));
		}
	}
	
	private void dispatch(String message, User user, Channel channel, boolean action) {
	    for (TriggerHandler handler: this.handlers) {
	        if (handler.isAction() != action) {
	            continue;
	        }
	        
	        Matcher m = handler.getPattern().matcher(message);
	        
	        if (!m.matches()) {
	            continue;
	        }
	        
	        TriggerInfo info = new TriggerInfo(user, channel, m, handler);
	        try {
	            handler.call(info);
	        } catch (Exception ex) {
	            Bot.logger.warning(String.format("Error handling trigger %s.", handler.getName()));
	            Bot.logger.throwing("Triggers", "onMsg", ex);
	        }
	    }
	}
	
	@Hook
	public void onAction(ActionEvent e) {
	    dispatch(e.getMessage(), e.getUser(), e.getChannel(), true);
	}
	
	@Hook
	public void onMsg(MessageEvent e) {
	    dispatch(e.getMessage(), e.getUser(), e.getChannel(), false);
	}
}