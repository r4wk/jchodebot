package at.mrschlauch.chodebot.plugins.irclog;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class Config {
    private List<String> channels = new ArrayList<String>();

    public Config() {
    }
}
