package at.mrschlauch.chodebot.plugins.irclog;

import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.events.ConnectEvent;
import at.mrschlauch.chodebot.events.DisconnectEvent;
import at.mrschlauch.chodebot.events.EventRegistry;
import at.mrschlauch.chodebot.events.Hook;
import at.mrschlauch.chodebot.plugins.Init;
import at.mrschlauch.chodebot.plugins.Plugin;
import at.mrschlauch.chodebot.plugins.PluginUtils;

@Plugin(name = "IRCLog", description = "Logs to IRC channels.")
public class IRCLog {
    private Config config;
    private IRCLogHandler handler;
    
    public IRCLog() {}
    
    @Init
    public void init() {
        this.config = PluginUtils.loadOrCreateConfig("IRCLog", Config.class, new Config());
        EventRegistry.getInstance().registerHandlers(this);
    }
    
    @Hook
    public void onConnect(ConnectEvent e) {
        this.handler = new IRCLogHandler(this.config.getChannels());
        Bot.logger.addHandler(this.handler);
    }
    
    @Hook
    public void onDisconnect(DisconnectEvent e) {
        Bot.logger.removeHandler(this.handler);
        this.handler = null;
    }
}
