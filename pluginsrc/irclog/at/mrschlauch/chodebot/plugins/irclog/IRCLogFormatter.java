package at.mrschlauch.chodebot.plugins.irclog;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import at.mrschlauch.chodebot.Colors;
import at.mrschlauch.chodebot.Utils;

public class IRCLogFormatter extends Formatter {
    private static Map<String,String> colorMap = new HashMap<String,String>();
    private static final int maxLevelNameLength = "WARNING".length();
    
    static {
        colorMap.put("SEVERE", Colors.RED);
        colorMap.put("WARNING", Colors.YELLOW);
        colorMap.put("INFO", Colors.GREEN);
        colorMap.put("CONFIG", Colors.BLUE);
        colorMap.put("FINE", Colors.BLUE);
        colorMap.put("FINER", Colors.BLUE);
        colorMap.put("FINEST", Colors.BLUE);
    }
    
    public IRCLogFormatter() {
    }

    @Override
    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder();
        String color = colorMap.get(record.getLevel().getName());
        builder.append(Colors.BOLD);
        builder.append(color);
        builder.append(Utils.center(record.getLevel().getName(), maxLevelNameLength));
        builder.append(Colors.NORMAL);
        builder.append(' ');
        builder.append(formatMessage(record));
        return builder.toString();
    }
}
