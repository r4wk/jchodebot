package at.mrschlauch.chodebot.plugins.irclog;

import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import at.mrschlauch.chodebot.Bot;
import at.mrschlauch.chodebot.Connection;

public class IRCLogHandler extends Handler {
    private List<String> channelNames;

    public IRCLogHandler(List<String> channelNames) {
        this.channelNames = channelNames;
        this.setFormatter(new IRCLogFormatter());
    }

    @Override
    public void close() throws SecurityException {
    }

    @Override
    public void flush() {
    }

    @Override
    public void publish(LogRecord record) {
        if (!isLoggable(record) || this.channelNames.isEmpty()) {
            return;
        }
        
        String formatted = this.getFormatter().format(record);
        Connection conn = Bot.getInstance().getConnection();
        
        for (String channelName: this.channelNames) {
            if (!conn.isOnChannel(channelName)) {
                continue;
            }
            conn.sendMessage(channelName, formatted);
        }
    }
}
