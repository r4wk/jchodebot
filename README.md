# jchodebot

jchodebot is my (probably futile) attempt at creating an IRC bot with
*working* user tracking for once. I've inspected various bots (skybot,
pircbotx, irclib, shocky), hoping to find a secure and useful
implementation of user tracking (keeping persistant track of user and
channel objects between messages, as well as user and channel metadata
like modes, topic, user statues etc). All had sketchy user tracking at
best, considering the corner cases I have found.

## Features of note
- User tracking
- A plugin framework inspired by the MinecraftForge Mod Loader for the game
  Minecraft
- A simple event system, which is coincidentally rather similar to the
  one used by the Bukkit server implementation of the game Minecraft.
- Bot user accounts based on NickServ accounts, for admin commands

## License
This software is released under the MIT license. See the LICENSE file for details.