# Todo list

- Add more events.
- Consider adding events for outgoing messages like Shocky has.
- Consider adding a permissions system like Bukkit or BioBot have,
  instead of admin or not user accounts.
- Pore over usertracking again, looking for corner cases.
  Especially involving learning user@hosts, bloody hidden hosts.
- Handle $#hostmask and $servermask notice/privmsg targets.
  enum TargetType maybe?
- Add an ant script for packaging the bot and its plugins into
  jars.
- Add a script for running the bot, instead of running via eclipse.
- Add jar plugin discovery.